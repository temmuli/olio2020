package com.example.SportsHall;

import java.util.ArrayList;

public class FeedbackControl {
    private static ArrayList<Feedback> feedbackList = new ArrayList<>();     //The list for controlling the feedbacks
    private static int runningId = 0;           //A running id to make the feedbacks unique

    private FeedbackControl() {}
    private static FeedbackControl control = new FeedbackControl();         //Feedback control is a static singleton class
    public static FeedbackControl getInstance() {
        return control;
    }

    public static void setRunningId(int i) { runningId = i; }       //Methdod to set the running id up to date
    public static int getRunningId() { return runningId; }

    public static ArrayList<Feedback> getFeedbackList() {
        return feedbackList;
    }

    public static void addToList(Feedback fb) {     //Getting a new feedback as input and adding it to the list and updating the xml file
        feedbackList.add(fb);
        saveToFile();               //The feedback xml file is updated every time a new feedback is added
    }
    public static int getUnreadFeedbackCount() {        //Method that returns the count of unread feedbacks, used to inform admin
        int count = 0;
        for (Feedback fb : feedbackList) {
            if (!fb.getReadStatus())
                count++;
        }
        return count;
    }

    public static Boolean removeFeedbackOnId(int id) {      //Method used to remove a feedback based on it's id
        for (Feedback fb : feedbackList) {
            if (fb.getId() == id) {
                feedbackList.remove(fb);
                saveToFile();                   //The file is updated when a feedback is removed
                return true;                    //We return true if the remove was successful
            }
        }
        return false;       //If the feedback id wasn't found, we return false
    }

    public static Boolean changeStatusOnId(int id) {        //Method to change the feedback's read/unread status based on id
        for (Feedback fb : feedbackList) {
            if (fb.getId() == id) {
                if (fb.getReadStatus()) {
                    fb.setReadStatus(false);
                } else if (!fb.getReadStatus()) {
                    fb.setReadStatus(true);
                }
                saveToFile();       //Updating the xml file after status change
                return true;
            }
        }
        return false;           //If the feedback id wasn't found, we return false
    }

    public static Boolean readToList() {        //Method to read the feedbacks from the xml file to feedback list
        ArrayList<Feedback> tempList = Xmlio.readFeedbackXML();     //Getting the feedback list from XMLIO's method
        if (tempList == null) {     //If none was found, we set the running id to 0 and return false
            setRunningId(0);
            return false;
        } else {                    //Otherwise clearing the old list and updating it with the found feedbacks
            feedbackList.clear();
            feedbackList.addAll(tempList);
            if (feedbackList.size() > 0)
                setRunningId(feedbackList.get(feedbackList.size()-1).getId());   //Setting the running id based on the list's last feedback's id
            return true;
        }
    }

    public static void saveToFile() {           //Method for writing the feedbacklist to xml file
        Xmlio.writeFeedbackXML(feedbackList);
    }
}
