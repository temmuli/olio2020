package com.example.SportsHall;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class FeedbackManagement extends AppCompatActivity {
    private Context context;
    private Feedback currentR, currentUR;
    private Spinner readSpinner, unreadSpinner;
    private ArrayList<Feedback> readSpinnerList, unreadSpinnerList;
    private TextView fbTV;
    private EditText messageET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_management);
        context = FeedbackManagement.this;

        fbTV = (TextView) findViewById(R.id.fbTV);
        messageET = (EditText) findViewById(R.id.messageET);
        messageET.setKeyListener(null);
        messageET.setFocusable(false);
        messageET.setCursorVisible(false);
        final Button deleteButton = (Button) findViewById(R.id.delete);
        final Button cancelButton = (Button) findViewById(R.id.cancel);
        deleteButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);          //Hiding the confirmation buttons until they are needed

        //The spinner for showing and selecting the read feedbacks
        readSpinnerList = new ArrayList<>();
        readSpinner = (Spinner) findViewById(R.id.readSpinner);
        ArrayAdapter<Feedback> readAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, readSpinnerList);
        readAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        readSpinner.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, readSpinnerList));
        readSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).toString().equals("Read feedbacks"))
                    setTVText();        //Showing a default message if feedback is not selected or found
                else {      //If feedbaack is selected, its information and message are shown
                    currentR = (Feedback) readSpinner.getItemAtPosition(readSpinner.getSelectedItemPosition());
                    for (Feedback fb : readSpinnerList)
                        if (fb.getId() == currentR.getId()) {
                            fbTV.setText("ID: " + currentR.getId() + " \nDate & time: " + currentR.getDate() + " \nUser: " + currentR.getUser()
                                    + "\nSportshall: " + currentR.getHallName() + "\nRead status: Read\nTitle: " + currentR.getTitle()+"\n\nMessage:");
                            messageET.setText(currentR.getText());
                        }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        //The spinner for showing and selecting the unread feedbacks
        unreadSpinnerList = new ArrayList<>();
        unreadSpinner = (Spinner) findViewById(R.id.unreadSpinner);
        ArrayAdapter<Feedback> unreadAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, unreadSpinnerList);
        unreadAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        updateSpinners();       //Method to get the feedbacks and updating them to spinners
        unreadSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).toString().equals("Unread feedbacks")) {
                    setTVText();
                } else {
                    currentUR = (Feedback) unreadSpinner.getItemAtPosition(unreadSpinner.getSelectedItemPosition());
                    for (Feedback fb : unreadSpinnerList)
                        if (fb.getId() == currentUR.getId()) {
                            fbTV.setText("ID: "+currentUR.getId()+" \nDate & time: "+currentUR.getDate() + " \nUser: " + currentUR.getUser()
                                    +"\nSportshall: "+currentUR.getHallName() +"\nRead status: Unread\nTitle: "+currentUR.getTitle()+"\n\nMessage:");
                            messageET.setText(currentUR.getText());
                        }
                 }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        //Button to change the selected feedbacks read/unread status, and updating the spinners
        final Button statusButton = (Button) findViewById(R.id.statusButton);
        statusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fbTV.getText().toString().equals("Feedback content") || fbTV.getText().toString().equals("No feedbacks available")) {
                    toast("Select a feedback");
                } else {
                    String[] text = fbTV.getText().toString().split(" ", 3);
                    FeedbackControl.changeStatusOnId(Integer.parseInt(text[1]));   //Using the FeedbackControl's method to change the status based on id
                    updateSpinners();
                    toast("Status changed");
                }
            }
        });

        //Button for removing the last selected feedback totally
        Button removeButton = (Button) findViewById(R.id.removeButton);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fbTV.getText().toString().equals("Feedback content") || fbTV.getText().toString().equals("No feedbacks available")) {
                    toast("Select a feedback");
                } else {
                    deleteButton.setVisibility(View.VISIBLE);   //Showing the confirmation buttons so that feedbacks aren't deleted by accident
                    cancelButton.setVisibility(View.VISIBLE);
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {       //Button to confirm the deletion
                String[] text = fbTV.getText().toString().split(" ", 3);
                    if (FeedbackControl.removeFeedbackOnId(Integer.parseInt(text[1]))) {
                        updateSpinners();
                        toast("Feedback removed");
                    } else {
                        toast("Remove failed");
                }
                deleteButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {       //Button for cancelling the action
                toast("Remove cancelled");
                deleteButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
            }
        });
    }
    @SuppressLint("SetTextI18n")
    public void updateSpinners() {      //Method for setting the read and unread spinners up to date
        readSpinnerList.clear();
        unreadSpinnerList.clear();
        ArrayList<Feedback> fbList = FeedbackControl.getFeedbackList();     //Getting the list of all feedbacks
        readSpinnerList.add(0, new Feedback("", "Read feedbacks","","", "")); //Setting the headers
        unreadSpinnerList.add(0, new Feedback("", "Unread feedbacks","","", ""));
        for (Feedback fb : fbList) {
            if (fb.getReadStatus()) {           //Ordering the feedbacks to right spinnerLists based on the read/unread status
                readSpinnerList.add(fb);
            } else if (!fb.getReadStatus()) {
                unreadSpinnerList.add(fb);
            }
        }
        readSpinner.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, readSpinnerList));
        unreadSpinner.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, unreadSpinnerList));
        setTVText();        //Showing a default message if feedback is not selected or found
    }

    @SuppressLint("SetTextI18n")
    public void setTVText() {       //Method for setting the default message in the text view field
        if (readSpinnerList.size() + unreadSpinnerList.size() == 2) {
            fbTV.setText("No feedbacks available");     //If the spinners only contain headers, no feedbacks are found
        } else {
            fbTV.setText("Feedback content");
        }
        messageET.setText("");
    }
    public void toast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBackPressed() {       //We return the count of unread feedbacks to admin's main menu on back pressed
        Intent intent = new Intent();
        intent.putExtra("unreadCount", FeedbackControl.getUnreadFeedbackCount());
        setResult(RESULT_OK, intent);
        finish();
    }
}
