package com.example.SportsHall;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.HashMap;
import java.util.List;

// Customized arrayAdapter for listView items, extending base class
public class CustomArrayAdapter extends ArrayAdapter<Reservation> {
    HashMap<Reservation, Integer> itemIDMap = new HashMap<Reservation, Integer>();

    // Constructor gets list of reservation objects, which are added to HashMap
    public CustomArrayAdapter(Context context, int textViewResourceId, List<Reservation> objects) {
        super(context, textViewResourceId, objects);
        for (int i = 0; i < objects.size(); i++) {
            itemIDMap.put(objects.get(i), i);
        }
    }

    @Override
    public long getItemId(int position) {
        Reservation res = getItem(position);
        return itemIDMap.get(res);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

}
