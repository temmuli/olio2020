package com.example.SportsHall;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Spinner detailSpinner;
    private TextView detailBox;
    private Context context;
    private static ArrayList<SportsHall> listOfHalls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;

        JsonIO IO = JsonIO.getInstance();

        // Read current list of sports, if empty --> add defaults as seen below
        try {
            IO.readSportsToList(SportControl.getSportList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Here we create some common default sports using the DefaultSport class
        if(SportControl.getSportList().isEmpty()) {
            DefaultSport basketball = new DefaultSport("Basketball", 20);
            DefaultSport futsal = new DefaultSport("Futsal", 25);
            DefaultSport handball = new DefaultSport("Handball", 25);
            DefaultSport badmington = new DefaultSport("Badminton", 12);
            DefaultSport floorball = new DefaultSport("Floorball", 25);
            SportControl.addToList(basketball);
            SportControl.addToList(futsal);
            SportControl.addToList(handball);
            SportControl.addToList(badmington);
            SportControl.addToList(floorball);
        }

        //Here we read the users from xml file to ArrayList
        if (!UserControl.readToList()) {    //If the file wasn't found, it is created and admin and test users added
            UserControl.addToList(new User("Admin", "admin@", "01010101010", "Admin12!", "Pallerohalli", true, false, true));
            UserControl.addToList(new User("Pekka", "testi@luukku.fi", "0401400140", "Pallerohalli", "Pallerohalli", false, false, true));
        }

        //Here we read the feedbacks from xml file to ArrayList
        if (!FeedbackControl.readToList()) {    //If the file wasn't found, it is created and a test feedback added
            FeedbackControl.addToList(new Feedback(1, "01/01/2000 00:00", "Auto test feedback", "This feedback is an automatically created test feedback", "Pallerohalli", "admin@", true));
        }

        //Here we create the three sportshalls to the system
        listOfHalls = new ArrayList<>();    //Arraylist has been created to manage sporthall objects (created below)
        SportsHall hall1 = new SportsHall("Pallerohalli", "Helsinki", "Rasmuksentie 12, Helsinki", "Mon-Fri 08-22\nSat 10-18\nSun CLOSED", "0503509381");
        SportsHall hall2 = new SportsHall("Jaminpallohalli", "Kuopio", "Jaminkatu 99, Kuopio", "Mon-Fri 08-22\nSat 10-19\nSun 10-14", "0503502206");
        SportsHall hall3 = new SportsHall("HjallisJethroAreena", "Lappeenranta", "Yliopistonkatu 34, Lappeenranta", "Mon-Fri 08-21\nSat 11-19\nSun 12-18", "05035012345");
        listOfHalls.add(hall1);
        listOfHalls.add(hall2);
        listOfHalls.add(hall3);

        //From this spinner user can choose a sportshall and see its contact information
        detailSpinner = (Spinner) findViewById(R.id.spinnerHallres);
        detailBox = (TextView) findViewById(R.id.detailsBox);
        hallDetails(listOfHalls);           //Method which governs spinner and hall details etc.

        //Buttons to go to login view or register view
        final Button registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goToRegister(listOfHalls);
            }
        });
        final Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLogin();
            }
        });
    }

    //The method for adding the sportshalls to the spinner and showing the contact info on click
    public void hallDetails(final ArrayList<SportsHall> listOfHalls) {
        ArrayAdapter<SportsHall> adapter =
                new ArrayAdapter<SportsHall>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, listOfHalls);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        detailSpinner.setAdapter(adapter);
        detailSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String name = detailSpinner.getSelectedItem().toString();
                for (SportsHall A : listOfHalls) {
                    if (A.getName().equals(name)) {
                        detailBox.setText("Address:\n" + A.getLocation() + "\n \nOpen:\n" + A.getOpeningHours() + "\n \nTel: " + A.getPhoneNumber());
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    public static ArrayList<SportsHall> getListOfHalls() {      //Method to get the list of Sportshall objects
        return listOfHalls;
    }

    public void goToLogin() {       //The method to send user to login view
        ArrayList<String> hallList = new ArrayList<>();
        for (SportsHall sh : listOfHalls)
            hallList.add(sh.getName());
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("list", hallList);          //We send the list of sportshall names as parameter
        startActivity(intent);
    }

    public void goToRegister(ArrayList<SportsHall> listOfHalls) {       //The method to send the user to registeration
        ArrayList<String> hallList = new ArrayList<>();
        for (SportsHall sh : listOfHalls)
            hallList.add(sh.getName());
        User user = new User();             //Here we create a new empty user-object which is sent to the register activity
        Intent intent = new Intent(this, RegisterActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("olio", user);
        bundle.putStringArrayList("list", hallList);    //We also send the list of hall names
        intent.putExtras(bundle);
        startActivityForResult(intent, 1);  //We want the registered user-object back from the register activity
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                User user = (User) data.getSerializableExtra("olio");
                UserControl.addToList(user);    //We catch the registered user object and add it to the list of users
            }
        }
    }
}
