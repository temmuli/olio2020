package com.example.SportsHall;

import android.annotation.SuppressLint;
import android.util.Xml;

import org.w3c.dom.Element;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Xmlio {

    private Xmlio() {}
    private static Xmlio xmlio = new Xmlio();                   //Xmlio is a static singleton class
    public static Xmlio getInstance() {
        return xmlio;
    }

    @SuppressLint("SdCardPath")     //We save the xml files in the device's data file, instead of a database just for this project
    private static String userFileLocation = "/data/data/com.example.monitoimihalli/users.xml";

    //Method to read the users from xml file to an ArrayList and return the list
    public static ArrayList<User> readUserXML() {
        ArrayList<User> tempList = new ArrayList<>();
        try {
            File userFile = new File(userFileLocation);     //Finding the user file
            if (userFile.exists()) {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = builder.parse(userFile);
                doc.getDocumentElement().normalize();
                NodeList userNodeList = doc.getDocumentElement().getElementsByTagName("user");  //All single user's info is under the tag "user"
                for (int i = 0; i < userNodeList.getLength(); i++) {
                    Node node = userNodeList.item(i);

                    if (node.getNodeType() == Node.ELEMENT_NODE) {      //Reading the nodes
                        Element element = (Element) node;               //Making element of the node

                        //Saving the user's information in variables based on the found tags
                        String name = element.getElementsByTagName("name").item(0).getTextContent();
                        String password = element.getElementsByTagName("password").item(0).getTextContent();
                        String email = element.getElementsByTagName("email").item(0).getTextContent();
                        String phone = element.getElementsByTagName("phone").item(0).getTextContent();
                        String homeCenter = element.getElementsByTagName("homeCenter").item(0).getTextContent();
                        Boolean isAdmin = Boolean.valueOf(element.getElementsByTagName("isAdmin").item(0).getTextContent());
                        Boolean locationConstraint = Boolean.valueOf(element.getElementsByTagName("locationConstraint").item(0).getTextContent());
                        Boolean isActive = Boolean.valueOf(element.getElementsByTagName("isActive").item(0).getTextContent());

                        //All info must be found to create a user
                        if (!name.isEmpty() && !password.isEmpty() && !email.isEmpty() && !phone.isEmpty() && !homeCenter.isEmpty() &&
                                !isAdmin.toString().isEmpty() && !locationConstraint.toString().isEmpty() && !isActive.toString().isEmpty()) {
                            User user = new User(name, email, phone, password, homeCenter, isAdmin, locationConstraint, isActive);
                            tempList.add(user);     //Creating a new user-object with the found data and adding it to the list
                        }
                    }
                }
                if (tempList.size() > 0) {
                    return tempList;
                } else {
                    return null;        //If the file was empty, we return null
                }
            } else if (!userFile.exists()) {    //If the file wasn't found, we return null
                return null;
            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method to write the given user list to xml file
    @SuppressLint("SdCardPath")
    public static void writeUserXML(ArrayList<User> userList) {
        try {
            File oldFile = new File(userFileLocation);      //Finding and renaming the old user file
            if (oldFile.exists()) {
                oldFile.renameTo(new File("/data/data/com.example.monitoimihalli/oldUserFile.xml"));
                oldFile.delete();
            }
            File userFile = new File(userFileLocation);     //Making a new file with the same name that the old one was
            userFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Writing the file with xml sreializer and file output stream
        try {
            XmlSerializer serializer = Xml.newSerializer();
            FileOutputStream fos = new FileOutputStream(userFileLocation);

            serializer.setOutput(fos, "UTF-8");
            serializer.startDocument("UTF-8", true);
            serializer.text("\n");
            serializer.startTag("", "Users");       //Tag that contains all users in between
            serializer.text("\n");

            for (User user : userList) {        //Going through the whole user list saving every user's info to the file
                serializer.text("\t");
                serializer.startTag("", "user");    //Tag for each user object, which contains all user's info
                serializer.text("\n");
                serializer.text("\t\t");
                serializer.startTag("", "name");
                serializer.text(user.getName());
                serializer.endTag("", "name");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "email");
                serializer.text(user.getEmail());
                serializer.endTag("", "email");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "phone");
                serializer.text(user.getPhone());
                serializer.endTag("", "phone");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "homeCenter");
                serializer.text(user.getHomeCenter());
                serializer.endTag("", "homeCenter");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "password");
                serializer.text(user.getPassword());
                serializer.endTag("", "password");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "isAdmin");
                serializer.text(user.getIsAdmin().toString());
                serializer.endTag("", "isAdmin");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "locationConstraint");
                serializer.text(user.getLocationConstraint().toString());
                serializer.endTag("", "locationConstraint");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "isActive");
                serializer.text(user.getIsActive().toString());
                serializer.endTag("", "isActive");
                serializer.text("\n");

                serializer.text("\t");
                serializer.endTag("", "user");      //Ending the user's tag
                serializer.text("\n");
            }
            serializer.endTag("", "Users");         //After all users are written, the Users tag is closed
            serializer.endDocument();                                //Ending the document
            serializer.flush();
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//####################################### Feedback xml control ##########################################################

    @SuppressLint("SdCardPath")     //We write the feedback file to the same directory as the user file, just for this project
    private static String feedbackFileLocation = "/data/data/com.example.monitoimihalli/feedbacks.xml";

    public static ArrayList<Feedback> readFeedbackXML() {
        ArrayList<Feedback> tempList = new ArrayList<>();
        try {
            File fbFile = new File(feedbackFileLocation);
            if (fbFile.exists()) {      //If the file is found, we read the found feedbacks to a list
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = builder.parse(fbFile);
                doc.getDocumentElement().normalize();
                NodeList fbNodeList = doc.getDocumentElement().getElementsByTagName("feedback");
                for (int i = 0; i < fbNodeList.getLength(); i++) {
                    Node node = fbNodeList.item(i);

                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;

                        String id = element.getElementsByTagName("id").item(0).getTextContent();
                        String date = element.getElementsByTagName("date").item(0).getTextContent();
                        String title = element.getElementsByTagName("title").item(0).getTextContent();
                        String text = element.getElementsByTagName("text").item(0).getTextContent();
                        String sportshall = element.getElementsByTagName("sportshall").item(0).getTextContent();
                        String user = element.getElementsByTagName("user").item(0).getTextContent();
                        Boolean readStatus = Boolean.valueOf(element.getElementsByTagName("readStatus").item(0).getTextContent());

                        if (!id.isEmpty()  && !date.isEmpty() && !title.isEmpty() && !text.isEmpty() && !sportshall.isEmpty()
                                && !readStatus.toString().isEmpty()) {
                            Feedback fb = new Feedback(Integer.parseInt(id), date, title, text, sportshall, user, readStatus);
                            tempList.add(fb);       //Here we create a new feedback on the found values and add it to the list
                        }
                    }
                }
                if (tempList.size() > 0) {
                    return tempList;
                } else {
                    return null;                        //If the file was empty, we return null
                }
            } else if (!fbFile.exists()) {              //If the file wasn't found, we return null
                return null;
            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Here we write the feedback list to a xml file
    @SuppressLint("SdCardPath")
    public static void writeFeedbackXML(ArrayList<Feedback> fbList) {
        try {
            File oldFile = new File(feedbackFileLocation);
            if (oldFile.exists()) {         //First we rename the old file if it was found
                oldFile.renameTo(new File("/data/data/com.example.monitoimihalli/oldFeedbackFile.xml"));
                oldFile.delete();
            }
            File fbFile = new File(feedbackFileLocation);       //Then we create a new file with the same name as last time
            fbFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            XmlSerializer serializer = Xml.newSerializer();     //Then we write the file with xml serializer and file output stream
            FileOutputStream fos = new FileOutputStream(feedbackFileLocation);

            serializer.setOutput(fos, "UTF-8");
            serializer.startDocument("UTF-8", true);
            serializer.text("\n");
            serializer.startTag("", "Feedbacks");       //Tag that contains all the feedbacks
            serializer.text("\n");

            for (Feedback fb : fbList) {        //We go through the list of all feedbacks
                serializer.text("\t");
                serializer.startTag("", "feedback");    //Tag that contains all data of one feedback
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "id");
                serializer.text(String.valueOf(fb.getId()));
                serializer.endTag("", "id");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "date");
                serializer.text(String.valueOf(fb.getDate()));
                serializer.endTag("", "date");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "readStatus");
                serializer.text(String.valueOf(fb.getReadStatus()));
                serializer.endTag("", "readStatus");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "sportshall");
                serializer.text(fb.getHallName());
                serializer.endTag("", "sportshall");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "user");
                serializer.text(fb.getUser());
                serializer.endTag("", "user");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "title");
                serializer.text(fb.getTitle());
                serializer.endTag("", "title");
                serializer.text("\n");

                serializer.text("\t\t");
                serializer.startTag("", "text");
                serializer.text(fb.getText());
                serializer.endTag("", "text");
                serializer.text("\n");

                serializer.text("\t");
                serializer.endTag("", "feedback");      //End of one feedback's tag
                serializer.text("\n");
            }
            serializer.endTag("", "Feedbacks");         //Ending all feedbacks when the list is fully read
            serializer.endDocument();                                    //Ending the document
            serializer.flush();
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
