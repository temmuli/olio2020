package com.example.SportsHall;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;

public class CheckReservationsActivity extends AppCompatActivity {
    private Context context;
    private User user;
    private String datePattern;
    private DateTimeFormatter formatter;
    private static ArrayList<Reservation> resList;
    private static CustomArrayAdapter adapter;
    private static CheckBox checkEmpty;
    private static ArrayList<String> hallTimes = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_reservations);

        // Declaring context and adding user through bundle
        context = CheckReservationsActivity.this;
        Bundle bundle = getIntent().getExtras();
        user = (User) bundle.getSerializable("olio");

        // Element declarations
        final Spinner sportSpinner = (Spinner) findViewById(R.id.sportSpinner);
        final Spinner hallSpinner = (Spinner) findViewById(R.id.constraintSpinner);
        final ListView LV = (ListView) findViewById(R.id.reservationLV);
        final Button searchButton = (Button) findViewById(R.id.filterButton);
        final EditText dateET = (EditText)findViewById(R.id.dateField);
        final TextView infoField = (TextView) findViewById(R.id.infoField);
        final Button nextD = (Button) findViewById(R.id.next);
        final Button prevD = (Button) findViewById(R.id.previous);
        checkEmpty = (CheckBox) findViewById(R.id.checkEmpty);

        // Adding a calendar item to use for date selection
        final Calendar dateSelection = Calendar.getInstance();
        final SimpleDateFormat format = new SimpleDateFormat("EEEE dd/MM/yyyy");

        // Listener for date picker, updating the clicked EditText field once set
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                dateSelection.set(Calendar.YEAR, year);
                dateSelection.set(Calendar.MONTH, monthOfYear);
                dateSelection.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dateET.setText(format.format(dateSelection.getTime()));
                if(!hallSpinner.getSelectedItem().toString().equals("No selection")){
                    SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
                    String dateString = format1.format(dateSelection.getTime());
                    hallTimes = MakeReservation.getTimeList(hallSpinner.getSelectedItem().toString(),dateString);
                }
            }
        };

        // Open a date picker when clicking the edittext
        dateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePick = new DatePickerDialog(context, date, dateSelection
                        .get(Calendar.YEAR), dateSelection.get(Calendar.MONTH),
                        dateSelection.get(Calendar.DAY_OF_MONTH));
                datePick.getDatePicker().setFirstDayOfWeek(Calendar.MONDAY);
                datePick.show();
            }
        });

        // OnClickListeners for next and previous day buttons, move to next/previous with dateSelection.add()
        nextD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelection.add(Calendar.DATE, 1);
                dateET.setText(format.format(dateSelection.getTime()));
            }
        });
        prevD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelection.add(Calendar.DATE, -1);
                dateET.setText(format.format(dateSelection.getTime()));
            }
        });

        // Date pattern and formatter for LocalDate
        datePattern = "^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/[0-9][0-9][0-9][0-9]$";
        formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        // Adding hall names as strings to spinner, adding "no selection" as default item
        final ArrayList<String> hallList = new ArrayList<String>();
        hallList.add("No selection");
        for(SportsHall sh:MainActivity.getListOfHalls()){
            hallList.add(sh.getName());
        }
        // Set hallSpinner to use hallAdapter with hallList as a parameter, set selection to "no selection"
        final ArrayAdapter<String> hallAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, hallList);
        hallAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        hallSpinner.setAdapter(hallAdapter);
        hallSpinner.setSelection(0);

        // Same as above but for sport-objects
        final ArrayList<String> sportList = new ArrayList<String>();
        sportList.add("No selection");
        for(Sport s:SportControl.getSportList()){
            sportList.add(s.getName());
        }
        ArrayAdapter<String> sportAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, sportList);
        sportAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sportSpinner.setAdapter(sportAdapter);
        sportSpinner.setSelection(0);


        // Declaring static adapter values
        try{
            adapter = new CustomArrayAdapter(context, android.R.layout.simple_list_item_1, resList);
        } catch (NullPointerException e){
            infoField.setText("Nothing selected.");
        }

        // Updating hallTimes- list when selecting a new hall from spinner
        hallSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!dateET.getText().toString().isEmpty() && !parent.getSelectedItem().toString().equals("No selection")) {
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    String dateString = format.format(dateSelection.getTime());
                    hallTimes = MakeReservation.getTimeList(parent.getSelectedItem().toString(),dateString);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // on click for searchbutton, filter the results of reservationlist based on selection
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!dateET.getText().toString().isEmpty()){
                    LocalDate date = dateSelection.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    if(!hallSpinner.getSelectedItem().toString().equals("No selection") && !sportSpinner.getSelectedItem().toString().equals("No selection")){
                        resList = filterByBoth(hallList.get(hallSpinner.getSelectedItemPosition()), date, sportList.get(sportSpinner.getSelectedItemPosition()));
                        adapter = new CustomArrayAdapter(context, android.R.layout.simple_list_item_1, resList);
                        infoField.setText("");
                        LV.setAdapter(adapter);
                    } else if(!hallSpinner.getSelectedItem().toString().equals("No selection")){
                        resList = filterByHall(hallList.get(hallSpinner.getSelectedItemPosition()), date);
                        adapter = new CustomArrayAdapter(context, android.R.layout.simple_list_item_1, resList);
                        infoField.setText("");
                        LV.setAdapter(adapter);
                    } else if(!sportSpinner.getSelectedItem().toString().equals("No selection")){
                        resList = filterBySport(sportList.get(sportSpinner.getSelectedItemPosition()), date);
                        adapter = new CustomArrayAdapter(context, android.R.layout.simple_list_item_1, resList);
                        infoField.setText("");
                        LV.setAdapter(adapter);
                    } else {
                        resList = filterByDate(date);
                        adapter = new CustomArrayAdapter(context, android.R.layout.simple_list_item_1, resList);
                        infoField.setText("");
                        LV.setAdapter(adapter);
                    }
                } else {
                    infoField.setText("Insert date.");
                }

                // Inform user if no results found
                if(LV.getCount() == 0 && !dateET.getText().toString().isEmpty()){
                    infoField.setText("No results on given criteria.");
                }
            }
        });

        // ListView onclick, when item is clicked display a alertdialog with reservation info
        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                TextView resInfoTV = new TextView(context);
                ImageView imageDisplay = new ImageView(context);
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Selected reservation");

                // New layout for alertDialog to display data as a string and the image
                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.HORIZONTAL);
                layout.setWeightSum(10f);

                // Adding views to layout, giving them weight as a parameter
                layout.addView(resInfoTV);
                layout.addView(imageDisplay);
                resInfoTV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,6f));
                imageDisplay.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,4f));

                // Set data for layout components
                resInfoTV.setText(resList.get(position).listData());
                imageDisplay.setImageResource(imageControl(resList.get(position).getImage()));

                builder.setView(layout);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                // A neutral button to join the selected reservation
                builder.setNeutralButton("Join", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Check if user can join and display new confirmation dialog if true
                        if(resList.get(position).canJoin(user)){
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                            builder1.setTitle("Join selected reservation?");
                            TextView resInfoTV1 = new TextView(context);
                            builder1.setView(resInfoTV1);
                            resInfoTV1.setText(resList.get(position).listData());

                            builder1.setPositiveButton("Join", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    addUser(resList.get(position),user);
                                    resList.get(position).addUser(user);
                                    Toast.makeText(context,"Joined successfully",Toast.LENGTH_LONG).show();
                                    LV.setAdapter(adapter);
                                }
                            });
                            builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder1.show();

                        } else {
                            Toast.makeText(context,"Cannot join selected reservation",Toast.LENGTH_LONG).show();
                        }
                    }
                });
                builder.show();
            }
        });
    }

    // Read current reservations to list and add to returnable list if matching to hall and date
    public ArrayList<Reservation> filterByHall(String sportsHall, LocalDate date){
        JsonIO IO = JsonIO.getInstance();

        // Temporary empty Sport to display as a part of empty reservations
        CreatedSport empty = new CreatedSport("Empty",0,"");

        try {
            IO.readToList(MainActivity.getListOfHalls(),SportControl.getSportList());
        } catch (JSONException | IOException | ParseException e) {
            e.printStackTrace();
        }

        ArrayList<Reservation> filteredResList = new ArrayList<Reservation>();

        // Loop through sportsHall-list, add either empty or existing Reservation to filtered list accordingly
        for(SportsHall s:MainActivity.getListOfHalls()) {
            if (s.getThisHallReservationList().isEmpty() && checkEmpty.isChecked() && s.getName().equals(sportsHall)) {
                for (String time : hallTimes) {
                    if (!time.equals("Select the time")) {
                        filteredResList.add(new Reservation(s.getName(), null, date, time, empty, false, null,"",null));
                    }
                }
                return filteredResList;
            } else if (s.getName().equals(sportsHall)){
                for (String time : hallTimes) {
                    for (Reservation r : s.getThisHallReservationList()) {
                        if (r.getTime().equals(time) && r.getDate().equals(date)) {
                            filteredResList.add(r);
                        }
                    }
                    if (checkEmpty.isChecked()) {
                        if (!time.equals("Select the time")) {
                            filteredResList.add(new Reservation(s.getName(), null, date, time, empty, false, null,"",null));
                        }
                    }
                }
                return filteredResList;
            }
        }
        return filteredResList;
    }

    // Read current reservations to list and add to returnable list if matching to date
    public ArrayList<Reservation> filterByDate(LocalDate date){
        JsonIO IO = JsonIO.getInstance();
        try {
            IO.readToList(MainActivity.getListOfHalls(),SportControl.getSportList());
        } catch (JSONException | IOException | ParseException e) {
            e.printStackTrace();
        }

        ArrayList<Reservation> filteredResList = new ArrayList<Reservation>();
        for(SportsHall sh:MainActivity.getListOfHalls()){
            for(Reservation r:sh.getThisHallReservationList()){
                if(r.getDate().equals(date)){
                    filteredResList.add(r);
                }
            }
        }
        return filteredResList;
    }

    // Read current reservations to list and add to returnable list if matching to sport
    public ArrayList<Reservation> filterBySport(String sport, LocalDate date){
        JsonIO IO = JsonIO.getInstance();
        try {
            IO.readToList(MainActivity.getListOfHalls(),SportControl.getSportList());
        } catch (JSONException | IOException | ParseException e) {
            e.printStackTrace();
        }

        ArrayList<Reservation> filteredResList = new ArrayList<Reservation>();
        for(SportsHall sh:MainActivity.getListOfHalls()){
            for(Reservation r:sh.getThisHallReservationList()){
                if(r.getSport().getName().equals(sport) && r.getDate().equals(date)){
                    filteredResList.add(r);
                } else if (checkEmpty.isChecked()){
                    continue;
                }
            }
        }
        return filteredResList;
    }

    // Read current reservations to list and add to returnable list if matching to all given data
    public ArrayList<Reservation> filterByBoth(String hall, LocalDate date, String sport){
        JsonIO IO = JsonIO.getInstance();
        try {
            IO.readToList(MainActivity.getListOfHalls(),SportControl.getSportList());
        } catch (JSONException | IOException | ParseException e) {
            e.printStackTrace();
        }

        ArrayList<Reservation> filteredResList = new ArrayList<Reservation>();
        for(SportsHall sH:MainActivity.getListOfHalls()){
            for(Reservation r:sH.getThisHallReservationList()){
                if(r.getDate().equals(date) && r.getSport().getName().equals(sport) && r.getSportsHallName().equals(hall)){
                    filteredResList.add(r);
                }
            }
        }
        return filteredResList;
    }

    // Add user to current reservation, write updated list to JSON-file
    public void addUser(Reservation res, User u){
        JsonIO IO = JsonIO.getInstance();

        for(SportsHall sh:MainActivity.getListOfHalls()){
            for(Reservation r:sh.getThisHallReservationList()){
                if(r == res){
                    r.addUser(u);
                    try {
                        IO.writeListToFile(MainActivity.getListOfHalls());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    // Match saved image-string to drawable resource to display as part of reservation
    public int imageControl(String image) {
        int imagePlace = 0;
        if (image.equals("Basketball")){
            imagePlace = getResources().getIdentifier("@drawable/basketball", null, this.getPackageName());
        } else if (image.equals("Badminton")) {
            imagePlace = getResources().getIdentifier("@drawable/badmington", null, this.getPackageName());
        } else if (image.equals("Floorball")) {
            imagePlace = getResources().getIdentifier("@drawable/floorball", null, this.getPackageName());
        } else if (image.equals("Futsal")) {
            imagePlace = getResources().getIdentifier("@drawable/futsal", null, this.getPackageName());
        } else if (image.equals("Handball")) {
            imagePlace = getResources().getIdentifier("@drawable/handball", null, this.getPackageName());
        } else if (image.equals("Happy sport man")) {
            imagePlace = getResources().getIdentifier("@drawable/happyman", null, this.getPackageName());
        } else {
            imagePlace = getResources().getIdentifier("@drawable/ic_launcher_foreground", null, this.getPackageName());
        }
        return imagePlace;
    }

}
