package com.example.SportsHall;

public class Feedback {
    private int id;
    private String date;
    private String title, text, hallName, user;
    private Boolean readStatus;     // false = unread, true = read

    //This constructor is used when user is creating a new feedback
    public Feedback(String date, String title, String text, String hallName, String user) {
        FeedbackControl.setRunningId(FeedbackControl.getRunningId()+1);    //Adding one to the running id when creating a new feedback
        this.id = FeedbackControl.getRunningId();
        this.date = date;
        this.title = title;
        this.text = text;
        this.hallName = hallName;
        this.user = user;
        this.readStatus = false;        //Setting the new feedback unread automatically
    }

    //This constructor is used when reading the feedbacks from the xml file
    public Feedback(int id, String date, String title, String text, String hallName, String user, Boolean readStatus) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.text = text;
        this.hallName = hallName;
        this.user = user;
        this.readStatus = readStatus;
    }

    public void setReadStatus(Boolean status) {
        this.readStatus = status;
    }

    public int getId() {
        return id;
    }
    public String getDate() {
        return date;
    }
    public String getTitle() {
        return title;
    }
    public String getHallName() {
        return hallName;
    }
    public String getUser() {
        return user;
    }
    public String getText() {
        return text;
    }
    public Boolean getReadStatus() { return readStatus; }

    @Override
    public String toString() {
        return title;
    }
}
