package com.example.SportsHall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

public class MainAdminActivity extends AppCompatActivity {
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    private TextView feedbackCountTV, userCountTV;
    private User admin, temp;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_admin);
        context = MainAdminActivity.this;

        admin = (User) getIntent().getSerializableExtra("olio");    //Gets the logged in admin-user as parameter
        toast("Welcome "+admin.getName());

        dl = (DrawerLayout)findViewById(R.id.activity_admin_main);      //Setting up the navigation drawer / side menu
        t = new ActionBarDrawerToggle(this, dl,R.string.Open, R.string.Close);
        dl.addDrawerListener(t);
        t.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userCountTV = (TextView) findViewById(R.id.userCountTV);        //Setting the count of active users and unread feedbacks to the screen
        setUserCount(UserControl.getActiveUserCount());
        feedbackCountTV = (TextView) findViewById(R.id.feedbackCountTV);
        setFbCount(FeedbackControl.getUnreadFeedbackCount());

        nv = (NavigationView)findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch(id)                                      //The side menu has all the possible actions for admin
                {                                               //which are:
                    case R.id.users:
                        goToUserManagement();                   //Viewing and controlling the users
                        break;
                    case R.id.reservations:
                        goToReservationManagement();            //Viewing and controlling the reservations
                        break;
                    case R.id.feedbacks:
                        goToFeedbackManagement();               //Viewing and controlling the feedbacks
                        break;
                    case R.id.account:
                        goToOwnProfile(admin);                  //Changing his own profile information
                        break;
                    case R.id.logout:                           //Logging out
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainAdminActivity.this);
                        builder.setTitle("Log out?");

                        builder.setPositiveButton("Log out", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {    //Confirmation of the logout
                                goToEntry();
                                finish();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {     //Cancelling the logout
                                dialog.cancel();
                            }
                        });
                        builder.show();
                        break;
                    default:
                        return true;
                }
                return true;
            }
        });
    }

    //Here are the methods for setting the counts on the screen
    @SuppressLint("SetTextI18n")
    public void setFbCount(int count) {
        feedbackCountTV.setText(count + " Unread feedbacks");
    }
    @SuppressLint("SetTextI18n")
    public void setUserCount(int count) {
        userCountTV.setText(count + " Active users");
    }

    //Here are the methods for opening the wanted view
    public void goToEntry(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    public void goToUserManagement() {
        Intent intent = new Intent(this, UserManagement.class);
        intent.putExtra("activeCount", UserControl.getActiveUserCount());   //We send the active user count to the UserManagement view
        startActivityForResult(intent, 2);
    }
    public void goToReservationManagement() {
        Intent intent = new Intent(this, AdminReservationManagement.class);
        startActivity(intent);
    }
    public void goToFeedbackManagement() {
        Intent intent = new Intent(this, FeedbackManagement.class);
        intent.putExtra("unreadCount", FeedbackControl.getUnreadFeedbackCount());   //We send the unread feedback count to the UserManagement view
        startActivityForResult(intent, 1);
    }
    public void goToOwnProfile(User admin){
        Intent intent = new Intent(this, AdminProfileActivity.class);
        try {
            temp = (User) admin.clone();               //We create and send a shallow copy of the admin-user to the own profile view
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        intent.putExtra("olio", temp);
        startActivityForResult(intent, 3);  //We also want the same copy back
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(t.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setFbCount(data.getExtras().getInt("unreadCount"));     //We set the updated count of unread feedbacks to the screen
            }                                                                //when the admin returns from the feedback management activity
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                setUserCount(data.getExtras().getInt("activeCount"));   //Same update for the active users
            }
        } else if (requestCode == 3) {                                          //When returning from the own profile activity,
            if (resultCode == RESULT_OK) {
                temp = (User) data.getSerializableExtra("olio");        //We catch the updated user-object
                if (UserControl.updateUserList(admin, temp)) {                 //We remove the old user and add the new one to user list
                    admin = temp;                                              //And we update the current user to the updated one
                    toast("Account updated");
                } else {
                    toast("Update failed");
                }
            }
        }
    }

    public void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
