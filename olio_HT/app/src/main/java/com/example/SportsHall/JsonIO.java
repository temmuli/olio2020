package com.example.SportsHall;

import android.annotation.SuppressLint;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.JsonWriter;

import org.json.JSONException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class JsonIO {
    private DateTimeFormatter formatter;

    // Class to static singleton
    private JsonIO() {}
    private static JsonIO jsonIO = new JsonIO();
    public static JsonIO getInstance() {return jsonIO; }

    // Declaring static file locations to be used in class
    @SuppressLint("SdCardPath")
    private static String reservationFileLocation = "/data/data/com.example.monitoimihalli/reservations.json";
    @SuppressLint("SdCardPath")
    private static String sportFileLocation = "/data/data/com.example.monitoimihalli/sports.json";

    // Writing reservations to JSON-file by looping through SportsHall-list
    @SuppressLint("SdCardPath")
    public void writeListToFile(ArrayList<SportsHall> shList) throws JSONException,IOException{

        // If reservation file exists, make a copy of it and delete
        try {
            File oldFile = new File(reservationFileLocation);
            if (oldFile.exists()) {
                oldFile.renameTo(new File("/data/data/com.example.monitoimihalli/oldRes.json"));
                oldFile.delete();
            }
            File userFile = new File(reservationFileLocation);
            userFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Declare JsonWriter with OutputStreamWriter to file location
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(new FileOutputStream(reservationFileLocation),"UTF-8"));
        writer.setIndent("\t");
        writer.beginArray();

        // Loop through hall objects and reservations of each hall, write accordingly
        for (SportsHall sh: shList) {
            writer.beginObject();
            writer.name("hall").value(sh.getName());
            ArrayList<Reservation> reservationList = sh.getThisHallReservationList();
            for (Reservation r : reservationList) {

                String creator = r.getCreator();
                String date = r.getDateString();
                String time = r.getTime();
                int userCount = r.getUserCount();
                String sportName = r.getSport().getName();
                String hallName = r.getSportsHallName();
                int maxPlayers = r.getSport().getMaxPlayers();
                Boolean isPrivate = r.getIsPrivate();
                String constrainedHall = r.getConstrainedHall();
                String image = r.getImage();


                writer.name("reservation");
                writer.beginObject();
                writer.name("hall").value(hallName);
                writer.name("creator").value(creator);
                writer.name("date").value(String.valueOf(date));
                writer.name("time").value(String.valueOf(time));
                writer.name("users").value(userCount);
                writer.name("sportName").value(sportName);
                writer.name("maxPlayers").value(maxPlayers);
                writer.name("isPrivate").value(isPrivate);
                writer.name("constrainedHall").value(constrainedHall);
                writer.name("image").value(image);
                writer.name("userList");
                writer.beginObject();
                for (User u : r.getUserList()){
                    writer.name("email").value(u.getEmail());

                }
                writer.endObject();
                writer.endObject();
            }
            writer.endObject();
        }
        writer.endArray();
        writer.close();
    }

    // Read reservation JSON file to each hall's reservation lists
    public void readToList(ArrayList<SportsHall> shList, ArrayList<Sport> sportList) throws JSONException, IOException, ParseException {

        // JsonReader with InputStreamReader on reservation file
        JsonReader reader = new JsonReader(new InputStreamReader(new FileInputStream(reservationFileLocation), "UTF-8"));
        String nextString = null;

        // Loop through halls, empty found reservation lists to avoid duplicate values
        for (SportsHall sH:shList){
            if(!sH.getThisHallReservationList().isEmpty()){
                sH.getThisHallReservationList().clear();
            }
        }

        // Read to each hall's reservation list
        for (SportsHall sh : shList) {

            ArrayList<Reservation> reservationList = sh.getThisHallReservationList();
            while (reader.hasNext()) {
                JsonToken next = reader.peek();
                if(next.equals(JsonToken.BEGIN_OBJECT)){
                    reader.beginObject();
                } else if (next.equals(JsonToken.NAME)){
                    nextString = reader.nextName();

                    // If name is reservation, parse it using readReservation method
                    if (nextString.equals("reservation")){
                        reservationList.add(readReservation(reader,sportList));
                    }
                } else if (next.equals(JsonToken.STRING)){
                    nextString = reader.nextString();
                } else if (next.equals(JsonToken.END_OBJECT)){
                    reader.endObject();
                } else if (next.equals(JsonToken.BEGIN_ARRAY)){
                    reader.beginArray();
                } else if (next.equals(JsonToken.END_ARRAY)) {
                    reader.endArray();
                }
            }
            reader.endObject();
        }
        reader.close();
    }

    // Read single reservation using previously declared reader
    public Reservation readReservation(JsonReader reader, ArrayList<Sport> sportList) throws IOException, ParseException {

        // Declare values and formatter for LocalDate
        formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String creator = null;
        LocalDate date = null;
        String time = null;
        int userCount = 0;
        String sportName = null;
        String hallName = null;
        int maxPlayers = 0;
        Boolean isPrivate = true;
        String constrainedHall = null;
        String image = "";
        ArrayList<String> emailList = new ArrayList<String>();

        String nextName = null;

        // Read a reservation object
        reader.beginObject();
        while (reader.hasNext()){
            String name = reader.nextName();
            if (name.equals("hall")){
                hallName = reader.nextString();
            } else if (name.equals("creator")) {
                creator = reader.nextString();
            } else if (name.equals("date")) {
                date = LocalDate.parse(reader.nextString(), formatter);
            } else if (name.equals("time")) {
                time = reader.nextString();
            } else if (name.equals("users")) {
                userCount = reader.nextInt();
            } else if (name.equals("sportName")) {
                sportName = reader.nextString();
            } else if (name.equals("maxPlayers")) {
                maxPlayers= reader.nextInt();
            } else if (name.equals("isPrivate")) {
                isPrivate= reader.nextBoolean();
            } else if (name.equals("constrainedHall")) {
                constrainedHall = reader.nextString();
            } else if (name.equals("image")){
                image = reader.nextString();
            } else if (name.equals("userList")) {

                // Read userList values
                while (reader.hasNext()){
                    JsonToken next = reader.peek();
                    if (next.equals(JsonToken.BEGIN_OBJECT)){
                        reader.beginObject();
                    } else if(next.equals(JsonToken.END_OBJECT)){
                        reader.endObject();
                    } else if (next.equals(JsonToken.NAME)) {
                        nextName = reader.nextName();
                        if (nextName.equals("email")) {
                            emailList.add(reader.nextString());
                        }
                    } else if (next.equals(JsonToken.STRING)){
                        String nextString = reader.nextString();
                    }
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        reader.endObject();

        // Loop through sportList to find a sport object matching read value
        Sport sport = null;
        for (Sport s:sportList){
            if(s.getName().equals(sportName)){
                sport = s;
            } else {
                continue;
            }
        }

        // Same as above, but for users
        ArrayList<User> userList = new ArrayList<User>();
        User user = null;
        for (User u:UserControl.getUserList()){
            for (String email:emailList){
                if(u.getEmail().equals(email)){
                    userList.add(u);
                }
            }

        }

        // Return new Reservation object with read values to add to list
        return new Reservation(hallName, creator, date, time, sport, isPrivate, constrainedHall,image,userList);
    }

    // Write sportList to JSON file
    public void writeSportsToFile(ArrayList<Sport> sportList) throws IOException, JSONException{

        // If file exists, copy it to oldFile and delete
        try {
            File oldFile = new File(sportFileLocation);
            if (oldFile.exists()) {
                oldFile.renameTo(new File("/data/data/com.example.monitoimihalli/oldSports.json"));
                oldFile.delete();
            }
            File userFile = new File(sportFileLocation);
            userFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // JsonWriter declaration
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(new FileOutputStream(sportFileLocation),"UTF-8"));
        writer.setIndent("\t");
        writer.beginArray();

        // Looping through sportList, writing objects
        for(Sport s:sportList){
            writer.beginObject();
            String name = s.getName();
            int maxP = s.getMaxPlayers();

            // Check the instance of iterator s to see if it is created or default, write using correct values for each
            if (s instanceof CreatedSport){
                String creator = ((CreatedSport) s).getCreator();
                writer.name("created");
                writer.beginObject();
                writer.name("creator").value(creator);
                writer.name("sportName").value(name);
                writer.name("maxPlayers").value(maxP);
                writer.endObject();
            } else {
                writer.name("default");
                writer.beginObject();
                writer.name("sportName").value(name);
                writer.name("maxPlayers").value(maxP);
                writer.endObject();
            }
            writer.endObject();
        }
        writer.endArray();
        writer.close();
    }

    // Reading from JSON file to sportList hosted on sportControl
    public void readSportsToList(ArrayList<Sport> sportList) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(new FileInputStream(sportFileLocation), "UTF-8"));

        // Check if list has any items, empty if yes to avoid duplicates
        if(!sportList.isEmpty()){
            sportList.clear();
        }

        String nextName = null;

        // Read file
        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            JsonToken next = reader.peek();
            if (next.equals(JsonToken.NAME)) {
                nextName = reader.nextName();

                // Check type of next sport object, direct to correct read-method
                if (nextName.equals("default")){
                    sportList.add(readDefault(reader));
                } else if (nextName.equals("created")) {
                    sportList.add(readCreated(reader));
                }
            }
            reader.endObject();
        }
        reader.endArray();
        reader.close();
    }

    // Read method for created sport using correct attributes
    public CreatedSport readCreated(JsonReader reader) throws IOException {
        String creator = null;
        String sportName = null;
        int maxPlayers = 0;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("creator")) {
                creator = reader.nextString();
            } else if (name.equals("sportName")) {
                sportName = reader.nextString();
            } else if (name.equals("maxPlayers")) {
                maxPlayers = reader.nextInt();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();

        // Returning new object to add to sportList
        return new CreatedSport(sportName,maxPlayers,creator);
    }

    // Reader for default sport objects using correct attributes
    public DefaultSport readDefault(JsonReader reader) throws IOException{
        String sportName = null;
        int maxPlayers = 0;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("sportName")) {
                sportName = reader.nextString();
            } else if (name.equals("maxPlayers")) {
                maxPlayers = reader.nextInt();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();

        // Returing new object to add to sportList
        return new DefaultSport(sportName,maxPlayers);
    }
}
