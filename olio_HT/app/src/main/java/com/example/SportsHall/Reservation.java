package com.example.SportsHall;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class Reservation {
    private String hallName, creator, time, constrainedHall;
    private LocalDate date;
    private Sport sport;
    private int userCount;
    private boolean isPrivate;      //isPrivate: true = private, false = open
    private String image;
    private ArrayList<User> userList;


    public Reservation(String hallName, String creator, LocalDate date, String time, Sport sport, Boolean isPrivate, String constrainedHall,String image, ArrayList<User> userList) {
        this.hallName = hallName;
        this.creator = creator;
        this.date = date;
        this.time = time;
        this.sport = sport;
        this.isPrivate = isPrivate;
        this.constrainedHall = constrainedHall;
        this.image = image;
        this.userList = userList;
    }

    public String getCreator() {
        return creator;
    }
    public LocalDate getDate() { return date; }

    // Return a formatted date as a string
    public String getDateString() { return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")); }
    public String getTime() { return time; }

    // Count the users of a reservation from userList, catch NullPointerException in case no users in the list
    public int getUserCount() {
        try {
            userCount = userList.size();
            return userCount;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return userCount = 0;
        }

    }
    public ArrayList<User> getUserList(){return userList;}

    public Sport getSport() {
        return sport;
    }
    public String getSportsHallName() {
        return hallName;
    }
    public boolean getIsPrivate() {
        return isPrivate;
    }
    public String getImage(){
        return image;
    }
    public String getConstrainedHall() { return constrainedHall; }


    public void setSport(Sport s){
        this.sport=s;
    }
    public void setCreator(String email){
        this.creator = email;
    }
    public void addUser(User user) {
        userList.add(user);
    }

    // Listing data for display
    public String listData(){
        return "Hall: "+"\t\t"+getSportsHallName()+"\n"+"Date: "+"\t\t"+getDate()+"\n"+"Time: "+"\t\t"+getTime()+"\n"+
                "Sport: "+"\t\t"+getSport().getName()+"\n"+"Max players: "+"\t\t"+getSport().getMaxPlayers()+"\n"+
                "Number of users: "+"\t\t"+getUserCount()+"\n"+"Constraint: "+"\t\t"+getConstrainedHall()+"\n";
    }

    @Override
    public String toString(){
        return getSport().getName()+", "+getDateString()+", "+getTime()+", "+getSportsHallName();
    }

    // Check if given user is eligible to join a reservation
    public Boolean canJoin(User user){
        try {
            for (User u:getUserList()){
                if (u.getEmail().equals(user.getEmail())){
                    return false;
                }
            }
            // Can only join if user is not in userList, home center is not constrained and reservation is not private
            if(this.getConstrainedHall().equals(user.getHomeCenter())){
                return false;
            } else if(this.getIsPrivate()) {
                return false;
            } else {
                return true;
            }
        // Check for NullPointerException if e.g. reservations userList is empty
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }
}
