package com.example.SportsHall;

import org.json.JSONException;

import java.io.IOException;
import java.io.Serializable;

public class User implements Serializable, Cloneable {
    private String name, email, phone, homeCenter, password;
    private Boolean isAdmin = false, locationConstraint = false, isActive = true;

    public User() {}

    //This builder is used when reading users from file to list
    public User(String name, String email, String phone, String password, String homeCenter, Boolean isAdmin, Boolean locationConstraint, Boolean isActive) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.homeCenter = homeCenter;
        this.isAdmin = isAdmin;
        this.locationConstraint = locationConstraint;
        this.isActive = isActive;
    }

    //This builder is used when new user registerates
    public User(String name, String email, String phone, String password, String homeCenter) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.homeCenter = homeCenter;
        this.isAdmin = false;
        if (homeCenter.equals("Jaminpallohalli")) {     //Users of Jaminpallohalli are constrained and can use only their home center
            this.locationConstraint = true;             //Other users can use all centers
        } else {
            this.locationConstraint = false;
        }
        this.isActive = true;
    }

    //Method for setting all the user's information at once
    public void setInformation(String name, String email, String phone, String password, String homeCenter) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.homeCenter = homeCenter;
        if (homeCenter.equals("Jaminpallohalli")) {     //Constraining the users of Jaminpallohalli
            this.locationConstraint = true;
        } else {
            this.locationConstraint = false;
        }
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    // Setting home center, if it equals Jaminpallohalli, we also set a constraint
    public void setHomeCenter(String homeCenter) {
        this.homeCenter = homeCenter;
        if (homeCenter.equals("Jaminpallohalli")) {
            this.locationConstraint = true;
        } else {
            this.locationConstraint = false;
        }
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setActivity(Boolean activity) {
        isActive = activity;
    }

    public String getName() { return name; }
    public String getEmail() { return email; }
    public String getPhone() { return phone; }
    public String getPassword() { return password; }
    public String getHomeCenter() { return homeCenter; }
    public Boolean getIsAdmin() { return isAdmin; }
    public Boolean getLocationConstraint() { return locationConstraint; }
    public Boolean getIsActive() { return isActive; }

    // String to display basic data in OwnProfile
    public String getAllDisplayableData(){
        return "Name: "+"\t\t"+getName()+"\n"+"Email: "+"\t\t"+getEmail()+"\n"+"Phone: "+"\t\t"+getPhone()+"\n";
    }

    // Get user data by integer, used to display user data in OwnProfile
    public String getDataByIndex(int index){
        switch (index){
            case 0:
                return name;
            case 1:
                return email;
            case 2:
                return phone;
            default:
                return "Choice not applicable";
        }
    }

    // Setting user data by integer, used in ownProfile data update
    public void setDataByIndex(int index, String data){
        switch (index){
            case 0:
                this.name=data;
                break;
            case 1:
                // If index matches updating email, update email also on reservations
                String oldEmail = this.email;
                this.email=data;
                updateEmailOnReservations(oldEmail,data);
                break;
            case 2:
                this.phone=data;
                break;
            default:
                break;
        }
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    // Update email on existing reservations when updating it for user
    public void updateEmailOnReservations(String email, String newEmail){
        for(SportsHall s:MainActivity.getListOfHalls()){
            for (Reservation r:s.getThisHallReservationList()){
                if(r.getCreator().equals(email)){
                    r.setCreator(newEmail);
                }
                for (User u:r.getUserList()){
                    if(u.getEmail().equals(email)){
                        u.setEmail(newEmail);
                    }
                }
            }
        }

        // Write updated reservations to JSON
        JsonIO IO = JsonIO.getInstance();
        try {
            IO.writeListToFile(MainActivity.getListOfHalls());
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }
}
