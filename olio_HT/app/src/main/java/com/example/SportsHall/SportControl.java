package com.example.SportsHall;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

public class SportControl {
    private static ArrayList<Sport> sportList = new ArrayList<>();     //Creating a list for controlling the sports

    public SportControl() {}
    private static SportControl control = new SportControl();         //SportControl is a static singleton class
    public static SportControl getInstance() {
        return control;
    }

    public static void addToList(Sport sport) {     //Method to add a new sport to the list and saving the list to the file
        sportList.add(sport);
        JsonIO IO = JsonIO.getInstance();
        try {
            IO.writeSportsToFile(sportList);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Sport> getSportList() {
        return sportList;
    }

    // Check if sport is already found in the list
    public static Boolean checkIfInList(Sport s){
        for (Sport sport:sportList){
            if (s.getName().equals(sport.getName())){
                return false;
            }
        } return true;
    }
}
