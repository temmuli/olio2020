package com.example.SportsHall;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MakeReservation extends AppCompatActivity{

    private EditText dateET, endDateET, sportName, maxPlayers;
    private TextView resBox, defaultHeader, newHeader;
    private Button confirmButton;
    private Spinner sportSpinner, hallsSpinner, timeSpinner, constraintSpinner, imageSpinner;
    private User user;
    private ArrayList<String> hallNameList, sportNameList, constraintList;
    private ArrayList<SportsHall> hallList;
    private ArrayList<Sport> sportList;
    private Switch sportSwitch, privateSwitch, continuousSwitch, constraintSwitch;
    private Context context;
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private DefaultSport defaultSport;
    private LocalDate date;
    private String time, hallName, creator, conHall, datePattern;
    private Boolean isPrivate, isContinuous;
    private JsonIO IO;
    private SimpleDateFormat ft;
    private SportsHall resHall;
    private ImageView imageView;

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_reservation);
        context = MakeReservation.this;

        try {
            Bundle bundle = getIntent().getExtras();
            user = (User) bundle.getSerializable("olio");    //Gets the logged in user as input
            hallNameList = bundle.getStringArrayList("list");    //Gets the list of hall names as input
        } catch (NullPointerException e) {
            e.printStackTrace();
            user = new User();
            hallNameList = new ArrayList<>();
        }

        datePattern = "^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/[0-9][0-9][0-9][0-9]$";   //The pattern used to check if the input date is valid
        hallList = MainActivity.getListOfHalls();          //Getting the list of hall objects
        constraintList = new ArrayList<>();                //The list used in constraining selected halls users
        constraintList.addAll(hallNameList);
        formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        privateSwitch = (Switch) findViewById(R.id.privateSwitch);
        defaultHeader = (TextView) findViewById(R.id.defaultHeader);
        maxPlayers = (EditText) findViewById(R.id.amountPlay);
        sportName = (EditText) findViewById(R.id.sportName);
        resBox =(TextView) findViewById(R.id.textBox);
        newHeader = (TextView) findViewById(R.id.newHeader);
        endDateET = (EditText) findViewById(R.id.endDateET);
        imageView = (ImageView) findViewById(R.id.imageView);

        newHeader.setVisibility(View.GONE);
        maxPlayers.setVisibility(View.GONE);
        sportName.setVisibility(View.GONE);                 //We set all later selections hidden until first ones are selected
        privateSwitch.setVisibility(View.GONE);
        defaultHeader.setVisibility(View.GONE);
        endDateET.setVisibility(View.GONE);
        imageView.setVisibility(View.GONE);

        sportList = SportControl.getSportList();            //Getting the list of default sports
        IO = JsonIO.getInstance();
        try {
            IO.readToList(MainActivity.getListOfHalls(),sportList);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        hallsSpinner = (Spinner) findViewById(R.id.spinnerHallres);     //The spinner where the hall for the reservation is selected
        hallNameList = checkConstraint(hallNameList, user);             //If the user is constrained, the list is shorter
        hallNameList.add(0, "Choose a sportshall");
        ArrayAdapter<String> homeAdapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, hallNameList);
        homeAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        hallsSpinner.setAdapter(homeAdapter1);
        hallsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (hallsSpinner.getItemAtPosition(position).equals("Choose a sportshall")) {
                    resBox.setText("Choose a sportshall");
                    dateET.setVisibility(View.GONE);
                    timeSpinner.setVisibility(View.GONE);
                    newHeader.setVisibility(View.GONE);
                    maxPlayers.setVisibility(View.GONE);
                    sportName.setVisibility(View.GONE);
                    privateSwitch.setVisibility(View.GONE);         //If the hall is not chosen, every else selection is hidden
                    sportSwitch.setVisibility(View.GONE);
                    continuousSwitch.setVisibility(View.GONE);
                    defaultHeader.setVisibility(View.GONE);
                    sportSpinner.setVisibility(View.GONE);
                    timeSpinner.setVisibility(View.GONE);
                    constraintSwitch.setVisibility(View.GONE);
                    constraintSpinner.setVisibility(View.GONE);
                    confirmButton.setVisibility(View.GONE);
                    endDateET.setVisibility(View.GONE);
                    imageSpinner.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);
                    constraintSwitch.setChecked(false);
                    sportSwitch.setChecked(false);
                    continuousSwitch.setChecked(false);
                    privateSwitch.setChecked(false);
                } else {
                    resBox.setText("");
                    dateET.setVisibility(View.VISIBLE);
                    dateET.setText(""); //If user switch hall after when he has chosen date and time, date pattern will be cleared and user must choose date again
                    timeSpinner.setVisibility(View.VISIBLE);
                    updateConstraintSpinner(constraintList, hallsSpinner.getSelectedItem().toString());  //Constraint spinner is updated based on the selected hall
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        ft = new SimpleDateFormat ("dd/MM/yyyy"); // we use this format when we compare userdate and today date.
        //We use this calendar class when we want to compare userdate and yesterday's date
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);

        dateET = (EditText) findViewById(R.id.dateET);      //The edit text field where user writes the date of his reservation
        dateET.setVisibility(View.GONE);
        dateET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -1);
                Date yesterday = calendar.getTime();
                if (s.toString().matches(datePattern)) {        //The given date must be in the wanted format
                    try {
                        Date date1 = ft.parse(s.toString());
                        if (yesterday.compareTo(date1) > 0) {   //The given date must be in the future
                            resBox.setText("Please give future date");
                        } else {                                //If the date is valid, the time spinner shows that days free timeslots
                            updateTimeSpinner(hallsSpinner.getSelectedItem().toString(), dateET.getText().toString());
                            resBox.setText("");
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    resBox.setText("Give valid date");
                    newHeader.setVisibility(View.GONE);
                    maxPlayers.setVisibility(View.GONE);
                    sportName.setVisibility(View.GONE);
                    privateSwitch.setVisibility(View.GONE);         //If the date is invalid, we hide all following selections
                    sportSwitch.setVisibility(View.GONE);
                    continuousSwitch.setVisibility(View.GONE);
                    defaultHeader.setVisibility(View.GONE);
                    constraintSwitch.setVisibility(View.GONE);
                    constraintSpinner.setVisibility(View.GONE);
                    endDateET.setVisibility(View.GONE);
                    sportSpinner.setVisibility(View.GONE);
                    confirmButton.setVisibility(View.GONE);
                    imageSpinner.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);
                    constraintSwitch.setChecked(false);
                    sportSwitch.setChecked(false);
                    continuousSwitch.setChecked(false);
                    privateSwitch.setChecked(false);
                }
            }
        });

        timeSpinner = (Spinner) findViewById(R.id.timeSpinner);         //The spinner which shows the free timeslots og the selected date
        timeSpinner.setVisibility(View.GONE);
        updateTimeSpinner(hallsSpinner.getSelectedItem().toString(), dateET.getText().toString());  //Spinner is updated in a method everytime the user changes the date
        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (timeSpinner.getItemAtPosition(position).equals("Select the time") || timeSpinner.getItemAtPosition(position).equals("Sportshall closed")) {
                    resBox.setText("Select the date & time");
                    newHeader.setVisibility(View.GONE);
                    maxPlayers.setVisibility(View.GONE);
                    sportName.setVisibility(View.GONE);
                    privateSwitch.setVisibility(View.GONE);         //If the time isn't chosen, all further selections are hidden
                    sportSwitch.setVisibility(View.GONE);
                    continuousSwitch.setVisibility(View.GONE);
                    defaultHeader.setVisibility(View.GONE);
                    constraintSwitch.setVisibility(View.GONE);
                    constraintSpinner.setVisibility(View.GONE);
                    endDateET.setVisibility(View.GONE);
                    sportSpinner.setVisibility(View.GONE);
                    confirmButton.setVisibility(View.GONE);
                    imageSpinner.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);
                    constraintSwitch.setChecked(false);
                    sportSwitch.setChecked(false);
                    continuousSwitch.setChecked(false);
                    privateSwitch.setChecked(false);
                } else {
                    resBox.setText("");
                    sportSwitch.setVisibility(View.VISIBLE);
                    defaultHeader.setVisibility(View.VISIBLE);      //If the timeslot is selected, next options are revealed
                    sportSpinner.setVisibility(View.VISIBLE);
                    privateSwitch.setVisibility(View.VISIBLE);
                    constraintSwitch.setVisibility(View.VISIBLE);
                    continuousSwitch.setVisibility(View.VISIBLE);
                    confirmButton.setVisibility(View.VISIBLE);
                    imageSpinner.setVisibility(View.VISIBLE);
                    imageView.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        sportSwitch = (Switch) findViewById(R.id.sportSwitch);      //The switch for choosing to use a default sport or to create a new one
        sportSwitch.setVisibility(View.GONE);
        sportSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    defaultHeader.setVisibility(View.GONE);
                    sportSpinner.setVisibility(View.GONE);      //If creating a new one, the default setting are hidden and custom settings revealed
                    newHeader.setVisibility(View.VISIBLE);
                    maxPlayers.setVisibility(View.VISIBLE);
                    sportName.setVisibility(View.VISIBLE);
                } else if (!isChecked) {
                    defaultHeader.setVisibility(View.VISIBLE);
                    sportSpinner.setVisibility(View.VISIBLE);   //If using a default, custom settings are hidden
                    newHeader.setVisibility(View.GONE);
                    maxPlayers.setVisibility(View.GONE);
                    sportName.setVisibility(View.GONE);
                }
            }
        });

        sportSpinner = (Spinner) findViewById(R.id.sportSpinner);   //The spinner where the user can choose the default sport for his reservation
        sportSpinner.setVisibility(View.GONE);
        sportNameList = new ArrayList<>();
        sportNameList.add("Choose a sport");
        for (Sport sp : sportList) {
            sportNameList.add(sp.getName()+", Max players: "+sp.getMaxPlayers());
        }
        ArrayAdapter<String> sportAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sportNameList);
        sportAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sportSpinner.setAdapter(sportAdapter);
        sportSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {}
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        constraintSwitch = (Switch) findViewById(R.id.constraintSwitch);    //The switch which lets the user to ban selected halls users from his reservation
        constraintSwitch.setVisibility(View.GONE);
        constraintSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    constraintSpinner.setVisibility(View.VISIBLE);
                    resBox.setText("Choose the constrained sportshall");
                } else {
                    constraintSpinner.setVisibility(View.GONE);
                    resBox.setText("");
                }
            }
    });

        constraintSpinner = (Spinner) findViewById(R.id.constraintSpinner);     //The spinner to choose the banned hall
        constraintSpinner.setVisibility(View.GONE);
        updateConstraintSpinner(constraintList, hallsSpinner.getSelectedItem().toString());

        //Here we create a spinner where the user can choose an image for his reservation
        ArrayList<String> imageList = new ArrayList<>();
        imageList.add("Choose an image");
        imageList.add("Basketball");
        imageList.add("Badminton");
        imageList.add("Floorball");
        imageList.add("Futsal");
        imageList.add("Handball");
        imageList.add("Happy sport man");
        imageSpinner = (Spinner) findViewById(R.id.imageSpinner);   //Setting up the spinner for images
        imageSpinner.setVisibility(View.GONE);
        final ArrayAdapter<String> imageAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, imageList);
        imageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        imageSpinner.setAdapter(imageAdapter);
        imageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                imageControl();     //Method that gets and adds the image
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        continuousSwitch = (Switch) findViewById(R.id.continuousSwitch);        //The switch for making a continuous reservation
        continuousSwitch.setVisibility(View.GONE);
        continuousSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    endDateET.setVisibility(View.VISIBLE);      //Setting visible the text box where the user can write
                } else if (!isChecked) {                        //the ending date of his continuous reservation
                    endDateET.setVisibility(View.GONE);
                }
            }
        });

        //The button to check all requirements and constraints and confirming the reservation
        confirmButton = (Button) findViewById(R.id.confirmButton);
        confirmButton.setVisibility(View.GONE);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                if (hallsSpinner.getSelectedItem().toString().equals("Choose a sportshall") ||
                        !dateET.getText().toString().matches(datePattern) || timeSpinner.getSelectedItem().toString().equals("Select the time")) {
                    resBox.setText("Check all info");
                    toast("Check all info");            //All info must be found before the reservation can be confirmed
                } else {
                    resBox.setText("");
                    hallName = hallsSpinner.getSelectedItem().toString();     //Determining the common attributes for all reservations
                    creator = user.getEmail();
                    isPrivate = privateSwitch.isChecked();      //isPrivate: true = private, false = open
                    isContinuous = continuousSwitch.isChecked();    //isContinuous true = continuous, false = one time
                    date = LocalDate.parse(dateET.getText().toString(), formatter);
                    time = timeSpinner.getSelectedItem().toString();
                    for (SportsHall sh : hallList) {
                        if (sh.getName().equals(hallName)) {
                            resHall = sh;               //Determining the selected hall object
                        }
                    }
                    if (constraintSwitch.isChecked()) {      //isChecked true = constrained, false = free for all
                        conHall = constraintSpinner.getSelectedItem().toString();
                    } else {
                        conHall = "No";
                    }

                    //If user wants to create his own sport for the reservation
                    if (sportSwitch.isChecked()) {
                        String namesport = sportName.getText().toString();
                        if ((maxPlayers.getText().toString().isEmpty()) && (namesport.isEmpty())) {
                            resBox.setText("Please enter Sport name and Max players");
                            toast("Check all info");
                        } else if (maxPlayers.getText().toString().isEmpty()) {         //The custom info must be found
                            resBox.setText("Please enter Max players");
                            toast("Check all info");
                        } else if (namesport.isEmpty()) {
                            resBox.setText("Please enter Sport name");
                            toast("Check all info");
                        } else {
                            int players = Integer.parseInt(maxPlayers.getText().toString());   //The max player count must be between 1-50
                            if (players == 0 || players > 50) {
                                resBox.setText("Max players must be between 1-50");
                                toast("Check all info");
                            } else {
                                resBox.setText("");
                                CreatedSport sport = new CreatedSport(namesport, players, creator);  // if everything is fine, new sport object is created by CreatedSport class
                                if(SportControl.checkIfInList(sport)) {
                                    SportControl.addToList(sport); //We add the created sport to our sport list for later use
                                }
                                if (isContinuous) {          //If the user is making a continuous reservation
                                    String endDateString = endDateET.getText().toString();
                                    if (!endDateString.matches(datePattern) || endDateString.isEmpty()) {   //The end date must match the pattern dd/MM/yyyy
                                        resBox.setText("Enter valid end date");
                                    } else {
                                        try {
                                            Date endDate = ft.parse(endDateString);     //Getting the inserted end date
                                            Date date2 = ft.parse(dateET.getText().toString()); //Getting the start date
                                            if (date2.compareTo(endDate) > 0){                      //The end date must be after the beginning date
                                                resBox.setText("End date must be after the beginning date");
                                            } else {
                                                resBox.setText("Checking reservations, please wait");
                                                ArrayList<String> continuousList = getContinuousDateList(date, endDate);    //Getting the list of all dates between start and end dates
                                                if (getContinuousFreeStatus(hallName, hallList, continuousList, time)) {    //Checking if all the days are free
                                                    resBox.setText("Saving reservations, please wait");
                                                    for (String d : continuousList) {              //Making the continuous reservation
                                                        LocalDate ld = LocalDate.parse(d, formatter);
                                                        ArrayList<User> userList = new ArrayList<User>();   //Adding the creator to the list of users attending the reservation
                                                        userList.add(user);
                                                        String image = imageSpinner.getSelectedItem().toString();
                                                        Reservation res = new Reservation(hallName, creator, ld, time, sport, isPrivate, conHall,image, userList); //New reservation is created for each date
                                                        resHall.makeReservation(res);     //Making the reservation to the selected sportshall
                                                    }
                                                    toast("Confirmed");
                                                    resBox.setText("Continuous reservation confirmed");
                                                    finish();       //All reservations are confirmed and we return the user to main menu
                                                } else {    //If there is already a reservation on one of the wanted dates and times, the confirmation is cancelled
                                                    resBox.setText("There is already a future reservation for the wanted time\n" +
                                                            "Try another date, time or sportshall");
                                                }
                                            }
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else {        //If user is making only one time custom sport reservation
                                    ArrayList<User> userList = new ArrayList<>();
                                    userList.add(user);
                                    String image = imageSpinner.getSelectedItem().toString();
                                    Reservation res = new Reservation(hallName, creator, date, time, sport, isPrivate, conHall,image, userList); // new reservation is created
                                    resHall.makeReservation(res);
                                    toast("Confirmed");
                                    resBox.setText("Reservation confirmed");
                                    finish();   //The one time custom sport reservation is confirmed and the user is returned to main menu
                                }
                            }
                        }

                    //If the user wants to reserve a default sport time
                    } else if (!sportSwitch.isChecked()) {
                        if (sportSpinner.getSelectedItem().toString().equals("Choose a sport")) {   //The sport must be chosen
                            resBox.setText("Select a sport");
                            toast("Check all info");
                        } else {
                            String nameSport = sportSpinner.getSelectedItem().toString();
                            for (Sport sp : sportList) {
                                if ((sp.getName() + ", Max players: " + sp.getMaxPlayers()).equals(nameSport)) {
                                    defaultSport = new DefaultSport(sp);        //Creating the default sport for the reservation
                                }
                            }
                            if (isContinuous) {                                 //If user is making continuous reservation
                                String endDateString = endDateET.getText().toString();
                                if (!endDateString.matches(datePattern) || endDateString.isEmpty()) {
                                    resBox.setText("Enter valid end date");
                                } else {
                                    try {
                                        Date endDate = ft.parse(endDateString);     //Getting the inserted end date
                                        Date date2 = ft.parse(dateET.getText().toString()); //Getting the start date
                                        if (date2.compareTo(endDate) > 0) {     //The end date must be after the start date
                                            resBox.setText("End date must be after the beginning date");
                                        } else {
                                            resBox.setText("Checking reservations, please wait");
                                            ArrayList<String> continuousList = getContinuousDateList(date, endDate);    //Getting the list of dates between start and end date
                                            if (getContinuousFreeStatus(hallName, hallList, continuousList, time)) {    //Checking if all the wanted dates and times are free
                                                resBox.setText("Saving reservations, please wait");
                                                for (String d : continuousList) {       //Making the reservation for each day between start and end dates
                                                    LocalDate ld = LocalDate.parse(d, formatter);
                                                    ArrayList<User> userList = new ArrayList<User>();   //Adding the creator to the list of users attending the reservation
                                                    userList.add(user);
                                                    String image = imageSpinner.getSelectedItem().toString();
                                                    Reservation res = new Reservation(hallName, creator, ld, time, defaultSport, isPrivate, conHall,image, userList); //New reservation is created for each date
                                                    resHall.makeReservation(res);   //Reservation is saved to the chosen sportshall's reservation list
                                                }
                                                toast("Confirmed");
                                                resBox.setText("Continuous reservation confirmed");
                                                finish();   //All reservations are confirmed and user is returned back to main menu
                                            } else {
                                                resBox.setText("There is already a future reservation for the wanted time\n" +
                                                        "Try another date or time or sportshall");
                                                }
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {            //If user is making only one time default sport reservation
                                ArrayList<User> userList = new ArrayList<>();
                                userList.add(user);
                                String image = imageSpinner.getSelectedItem().toString();
                                Reservation res = new Reservation(hallName, creator, date, time, defaultSport, isPrivate, conHall,image,userList);
                                resHall.makeReservation(res);
                                toast("Confirmed");
                                resBox.setText("Reservation confirmed");
                                finish();       //Reservation is confirmed and user is returned back to main menu
                            }
                        }
                    }
                }
            }
        });
    }

    //Method to check if the user is constrained and removing blocked halls from the possible halls list
    public ArrayList<String> checkConstraint(ArrayList<String> hallNameList, User user) {
        if (user.getLocationConstraint()) {
            for (int i = 0; i < hallNameList.size(); i++) {
                if (!user.getHomeCenter().equals(hallNameList.get(i))) {
                    hallNameList.remove(i);         //If the user is constrained, he can only reserve a timeslot from his own hall
                }
            }
        }
        return hallNameList;        //Returning the updated hall list
    }

    public void updateConstraintSpinner(ArrayList<String> constraintList, String hallName) {    //Method that updates the constraint spinner
        ArrayList<String> tempList = new ArrayList<>();
        tempList.addAll(constraintList);
        for (int i = 0; i < tempList.size(); i++) {
            if (tempList.get(i).equals(hallName)) {        //The reserved hall's users can't be blocked
                tempList.remove(i);                       //Removing the hall which the reservation is going to be in from the list
            }
        }
        if (!tempList.get(0).equals("Choose a sportshall to be constrained")) {
            tempList.add(0, "Choose a sportshall to be constrained");
        }
        ArrayAdapter<String> constraintAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, tempList);
        constraintAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        constraintSpinner.setAdapter(constraintAdapter);    //Updating the spinner with the updated list
    }

    public void updateTimeSpinner(String hallName, String date) {       //Setting the timeSpinners free timeslots
        ArrayList<String> timeList = getTimeList(hallName, date);       //Getting the list of all timeslots when the hall is open
        if (!hallName.equals("Choose a sportshall") && dateET.getText().toString().matches(datePattern)) {
            for (SportsHall sh : hallList) {
                if (sh.getName().equals(hallName))
                    timeList = getFreeTimeList(sh, timeList, date);     //Getting the list of free timeslots
            }
        }
        ArrayAdapter<String> timeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, timeList);
        timeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeSpinner.setAdapter(timeAdapter);    //Setting the updated list the spinner
    }

    //Returns a list of given hall's opening hours of the searched date
    public static ArrayList<String> getTimeList(String hallName, String dateString) {
        ArrayList<String> timeList = new ArrayList<>();         //Creating an empty list for the timeslots
        if (dateString.isEmpty()) {
            timeList.add(0, "Select the time");     //If the date is not selected, an empty timelist is returned
            return timeList;
        }
        LocalDate date = LocalDate.parse(dateString, formatter);
        String dayName = date.format(DateTimeFormatter.ofPattern("EEE"));
        String[] dayAndHrs;
        for (SportsHall sh : MainActivity.getListOfHalls()) {
            if (sh.getName().equals(hallName)) {    //Getting the searched hall
                String[] hrsPerDays = sh.getOpeningHours().split("\n", 4);  //Splitting the opening days
                if (dayName.equals("Sun")) {                                        //First checking the day of the week
                    dayAndHrs = hrsPerDays[2].split(" ", 2);            //Then getting the time window
                } else if (dayName.equals("Sat")) {
                    dayAndHrs = hrsPerDays[1].split(" ", 2);
                } else {                                                            //Mon-Fri
                    dayAndHrs = hrsPerDays[0].split(" ", 2);
                }
                if (dayAndHrs[1].equals("CLOSED")) {                                //If the hall is closed, only one option is sent
                    timeList.add("Sportshall closed");
                } else {
                    timeList = getSeparateTimes(dayAndHrs[1], timeList);            //Converting the time window to a list of timeslots
                    timeList.add(0, "Select the time");              //Adding the header
                }
                return timeList;        //Returning the updated timelist
            }
        }
        return null;            //If something fails, we return a null
    }

    //Changes the given opening hours timewindow to an ArrayList of timeslots
    public static ArrayList<String> getSeparateTimes(String timeWindow, ArrayList<String> timeList) {
        int a = 0, b = 0;
        if (timeList.size() > 0)        //If the list isn't empty, it is cleared
            timeList.clear();
        if (timeWindow.contains("-")) {
            timeWindow = timeWindow.replaceAll("\\.00", "");     //Unnecessary zeros and spaces are removed
            timeWindow = timeWindow.replaceAll(" ", "");
            String[] search = timeWindow.split("-");                        //Splitting the time window to the start(a) and end(b)
            String[] hours = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
            for (int i = 0; i < hours.length; i++) {
                if (hours[i].equals(search[0])) {
                    a = i;                                      //Determining the starting hour
                }
                if (hours[i].equals(search[1])) {
                    b = i;                                      //Determining the ending hour
                }
            }
            for (int x = a; x <= b-1; x++) {                    //Making the list of all the timeslots between a and b
                timeList.add(hours[x]+"-"+hours[x+1]);          //Timeslots are in format: XX-XX
            }
        }
        return timeList;        //Returning the list of timeslots inside the timewindow
    }

    //Returns a list of unbooked times of the given hall on the searched date
    public ArrayList<String> getFreeTimeList(SportsHall sh, ArrayList<String> timeList, String date) {
        for (Reservation res : sh.getThisHallReservationList()) {   //Goes through the selected halls reservation list
            if (res.getDateString().equals(date)) {     //If the found resevation's date is same with the searched date, we have to check the time
                for (int i = 0; i < timeList.size(); i++) {
                    if (timeList.get(i).equals(res.getTime())) {     //If there is a reservation in same hall on same date at the same time
                        timeList.remove(i);                         //that time is removed from the possible time options list
                    }
                }
            }
        }
        return timeList;        //The updated list of free timeslots is returned
    }

    //Returns a list of all same weekdates between the given start and end dates
    public ArrayList<String> getContinuousDateList(LocalDate localDate, Date endDate) {
        ArrayList<String> continuousList = new ArrayList<>();   //List of the continuous dates is created
        int week = 7;
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());  //The start date is the given date
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);     //The calendar is set to the start date
        do {
            continuousList.add(ft.format(date));       //Parsing the date as dd/MM/yyyy to the list
            calendar.add(Calendar.DAY_OF_YEAR, week);   //Going to next week's same weekday
            date = calendar.getTime();
        } while(date.before(endDate));               //Until we cross the end date set by user
        return continuousList;      //The list of dates is returned
    }

    //The method that checks if all the given continuous dates and timeslots are free
    public Boolean getContinuousFreeStatus(String hallName, ArrayList<SportsHall> hallList, ArrayList<String> continuousList, String time) {
        for (SportsHall sh : hallList) {
            if (sh.getName().equals(hallName)) {
                ArrayList<Reservation> resList = sh.getThisHallReservationList();   //Getting the reservation list of the selected hall
                for (String date : continuousList) {        //Going through all dates in the continuous date list
                    for (Reservation res : resList) {       //Checking all reservations in the hall's reservation list
                        if (res.getDateString().equals(date)) {
                            if (res.getTime().equals(time)) {       //Returns false if there is already a reservation on the same hall
                                return false;                       //on the same date on the same time
                            }
                        }
                    }
                }
            }
        }
        return true;                                                //Returns true if all wanted timeslots are available
    }

    public void imageControl() {        //Method to set an image to the reservation
        String image = imageSpinner.getSelectedItem().toString();
        int imagePlace = 0;
        if (image == "Basketball"){
            imagePlace = getResources().getIdentifier("@drawable/basketball", null, this.getPackageName());
        } else if (image == "Badminton") {
            imagePlace = getResources().getIdentifier("@drawable/badmington", null, this.getPackageName());
        } else if (image == "Floorball") {
            imagePlace = getResources().getIdentifier("@drawable/floorball", null, this.getPackageName());
        } else if (image == "Futsal") {
            imagePlace = getResources().getIdentifier("@drawable/futsal", null, this.getPackageName());
        } else if (image == "Handball") {
            imagePlace = getResources().getIdentifier("@drawable/handball", null, this.getPackageName());
        } else if (image == "Happy sport man") {
            imagePlace = getResources().getIdentifier("@drawable/happyman", null, this.getPackageName());
        } else {
            imagePlace = getResources().getIdentifier("@drawable/ic_launcher_foreground", null, this.getPackageName());
        }
        imageView.setImageResource(imagePlace);
    }

    public void toast(String msg) {     //The method used to inform user by toasting
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}