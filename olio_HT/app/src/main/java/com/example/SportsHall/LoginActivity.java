package com.example.SportsHall;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    private Button buttonLogin, randomNumButton;
    private EditText passwordLogin, emailLogin, randomNumBox;
    private TextView infoText, randomNumText, wrongNumText, insertText;
    private ArrayList<String> hallList;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;

        hallList = getIntent().getStringArrayListExtra("list");    //Gets sportshall name list as input
        randomNumButton = findViewById(R.id.randomNumButton);
        randomNumBox = findViewById(R.id.randomNum);
        randomNumText = findViewById(R.id.randomNunTex);
        insertText = findViewById(R.id.insertText);
        wrongNumText = findViewById(R.id.wrongNumTex);
        emailLogin = findViewById(R.id.emailLogin);
        passwordLogin = findViewById(R.id.passwordLogin);
        buttonLogin = findViewById(R.id.buttonLogin1);
        infoText = findViewById(R.id.informationTextView);
        randomNumText.setVisibility(View.GONE);
        randomNumBox.setVisibility(View.GONE);
        randomNumButton.setVisibility(View.GONE);
        insertText.setVisibility(View.GONE);

        //Button that checks the email and password and either lets user go forward or try again
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                String emailNow = emailLogin.getText().toString();
                String passwordNow = passwordLogin.getText().toString();
                ArrayList<User> userList = UserControl.getUserList();       //Gets the list of all registered users from UserControl
                try {
                    for (int i = 0; i < userList.size(); i++) {
                        if ((userList.get(i).getEmail().equals(emailNow)) && (userList.get(i).getPassword().equals(passwordNow))) { //if these terms come true, user will move on
                            if (userList.get(i).getIsActive()) {        //The user's account has to be active in order to move on
                                infoText.setText("Please enter the following number to the text field");
                                randomNumbers(userList.get(i));                 // user-object goes in randomNumbers method
                                break;
                            } else if (!userList.get(i).getIsActive()) {        // if account has been frozen, user can't login
                                infoText.setText("Your account has been frozen\nPlease contact admin@monitoimihalli.fi for more information");
                                break;
                            }
                        } else if (i == userList.size() - 1) {      //Message, if the matching email and password weren't found in the list
                            infoText.setText("Invalid email and/or password.\nPlease, try again.");
                        }
                    }
                } catch (NullPointerException e) {
                    System.out.println(e + "\nEmpty list etc.");
                }
            }
        });
    }

    //Method to get 6 random numbers and make user write them to a text box. If correct, login is completed
    public void randomNumbers(final User user){
        randomNumButton.setVisibility(View.VISIBLE);
        randomNumBox.setVisibility(View.VISIBLE);
        randomNumText.setVisibility(View.VISIBLE);
        insertText.setVisibility(View.VISIBLE);
        int one = getNumber(); // Six variables are used in the confirmation code
        int two = getNumber();
        int three = getNumber();
        int four = getNumber();
        int five = getNumber();
        int six = getNumber();
        final String combination = String.valueOf(one) + String.valueOf(two) + String.valueOf(three) + String.valueOf(four)
                + String.valueOf(five) + String.valueOf(six);       //Here we make the combination
        randomNumText.setText(combination);
        randomNumButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                if (combination.equals(randomNumBox.getText().toString())){     //If confirm code is correct
                    if (user.getIsAdmin()) {                //If user is admin, he goes to admin menu
                        infoText.setText("Admin login confirmed");
                        goToAdminMain(user);
                        finish();
                    }
                    else if (!user.getIsAdmin()) {          //If user is non-admin, he goes to main menu
                            infoText.setText("Login confirmed");
                            goToNormalMain(user);
                            finish();
                        }
                } else {
                    wrongNumText.setText("Wrong combination, try again.");
                }
            }
        });
    }
    public int getNumber(){   //Here is our function which always returns a random number between 0-9
        int max = 9;
        int min = 0;
        int range = max - min + 1;
        int num = (int) (Math.random() * range) + min;
        return num;
    }

    public void goToNormalMain(User user) {     //Method to send the user to main menu
        Intent intent = new Intent(this, MainNormalActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("olio", user);
        bundle.putStringArrayList("list", hallList);        //We send the logged in user and hall name list as parameters
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void goToAdminMain(User user) {      //Method to send the admin-user to admin menu
        Intent intent = new Intent(this, MainAdminActivity.class);
        intent.putExtra("olio", user);
        startActivity(intent);
    }
}
