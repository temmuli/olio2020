package com.example.SportsHall;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class OwnProfileActivity extends AppCompatActivity {
    private User user;
    private Context context;
    private EditText newPassFirst, newPassSecond, oldPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_own_profile);

        // Set context and user
        context = OwnProfileActivity.this;
        user = (User) getIntent().getSerializableExtra("olio");

        final TextView ownDataTV = (TextView) findViewById(R.id.ownData);
        final TextView homeCenterTV = (TextView) findViewById(R.id.HomeCenter);

        // Make string of basic user data, display it in TextView
        String userData = user.getAllDisplayableData();
        ownDataTV.setText(userData);

        // Display users home center
        String homeCenter = "Home center: "+"\t\t"+user.getHomeCenter();
        homeCenterTV.setText(homeCenter);

        // Button to modify basic data, displays adapter including items
        final Button mod = (Button) findViewById(R.id.modButton);
        final String[] dataItems = {"Name", "Email", "Phone"};
        mod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select data to update");
                builder.setSingleChoiceItems(dataItems,0,null);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        // Display new dialog asking for new value for attribute selected previously
                        final int selected = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                        builder2.setTitle("Updating "+dataItems[selected]+", old value: "+user.getDataByIndex(selected));

                        final EditText input = new EditText(context);
                        input.setHint("New Value: ");
                        input.setInputType(InputType.TYPE_CLASS_TEXT);
                        builder2.setView(input);

                        // Update user data by index, update TextView to match made changes
                        builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                user.setDataByIndex(selected,input.getText().toString());
                                ownDataTV.setText(user.getAllDisplayableData());
                            }
                        });
                        builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder2.show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

        // Display a dialog for change in home center, which is selectable from a spinner
        final Button mod2 = (Button) findViewById(R.id.modButton2);
        mod2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Spinner hallSelection = new Spinner(context);
                final ArrayAdapter<SportsHall> itemList = new ArrayAdapter<SportsHall>(context, android.R.layout.simple_spinner_item, MainActivity.getListOfHalls());
                hallSelection.setAdapter(itemList);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select new home center below, current hall: "+user.getHomeCenter());
                builder.setView(hallSelection);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        user.setHomeCenter(hallSelection.getSelectedItem().toString());
                        homeCenterTV.setText("Home center: "+"\t\t"+user.getHomeCenter());
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

        // Button to modify password, opens a dialog
        final Button modPassword = (Button) findViewById(R.id.modButton3);
        modPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Change password");

                // Dialog asks for old password
                oldPassword = new EditText(context);
                oldPassword.setHint("Current password:");
                oldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                builder.setView(oldPassword);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Comparing given password to users current password, if correct, open a new dialog
                        if (oldPassword.getText().toString().equals(user.getPassword())) {
                            AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                            builder2.setTitle("Give new password");

                            // New layout to display two EditText fields in dialog
                            LinearLayout layout = new LinearLayout(context);
                            layout.setOrientation(LinearLayout.VERTICAL);
                            newPassFirst = new EditText(context);
                            newPassFirst.setHint("New password:");
                            newPassFirst.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            newPassSecond = new EditText(context);
                            newPassSecond.setHint("New password again:");
                            newPassSecond.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            layout.addView(newPassFirst);
                            layout.addView(newPassSecond);

                            builder2.setView(layout);

                            builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Compare given password to each other and use method checkPassword to see validity, update if ok
                                    String newPass1 = newPassFirst.getText().toString();
                                    String newPass2 = newPassSecond.getText().toString();
                                    if (RegisterActivity.checkPassword(newPass1) && newPass1.equals(newPass2)) {
                                        Toast.makeText(context,"Password changed!",Toast.LENGTH_LONG).show();
                                        user.setPassword(newPass1);
                                    } else {
                                        Toast.makeText(context,"Password change failed!",Toast.LENGTH_LONG).show();
                                        dialog.cancel();
                                    }
                                }
                            });

                            builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder2.show();
                        } else {
                            Toast.makeText(context,"Wrong password",Toast.LENGTH_LONG).show();
                            dialog.cancel();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

    }
    @SuppressLint("SetTextI18n")
    @Override
    // When going back, pass back the updated user object
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("olio", user);
        setResult(RESULT_OK, intent);
        finish();
    }
}
