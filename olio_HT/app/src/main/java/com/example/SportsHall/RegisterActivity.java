package com.example.SportsHall;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements Serializable {
    private ArrayList<String> hallList;
    private User temp;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        context = RegisterActivity.this;

        try {
            Bundle bundle = getIntent().getExtras();
            temp = (User) bundle.getSerializable("olio");    //Gets a new empty object as input
            hallList = bundle.getStringArrayList("list");    //Gets the list of hall names as input
            hallList.add(0, "Choose primary sportshall");
        } catch (NullPointerException e) {
            e.printStackTrace();
            temp = new User();
            hallList = new ArrayList<>();
            hallList.add("Choose primary sportshall");
        }

        final TextView screen = (TextView) findViewById(R.id.screen);
        final EditText nameET = (EditText) findViewById(R.id.name);
        final EditText emailET = (EditText) findViewById(R.id.email);
        final EditText phoneET = (EditText) findViewById(R.id.phone);

        final Spinner homeSpinner = (Spinner) findViewById(R.id.homeCenterSpinner);     //Spinner where user chooses his primary sportshall
        ArrayAdapter<String> homeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, hallList);
        homeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        homeSpinner.setAdapter(homeAdapter);

        final EditText password1ET = (EditText) findViewById(R.id.password1);
        password1ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                if (checkPassword(s.toString()))                //Password is checked after it has been changed
                    screen.setText("Valid password");
                else                                            //If the password is invalid, user gets the requirements on the screen
                    screen.setText("Password must be at least 12 characters and contain:\n" +
                            "a minimum of 1 lower case letter [a-z] and\n" +
                            "a minimum of 1 upper case letter [A-Z] and\n" +
                            "a minimum of 1 numeric character [0-9] and\n" +
                            "a minimum of 1 special character: [~`!@#$%^&*()-_+={}[]|\\;:\"<>,./]");
            }
        });

        final EditText password2ET = (EditText) findViewById(R.id.password2);
        password2ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                if (comparePasswords(password1ET.getText().toString(), s.toString()))       //Passwords are compared after writing them
                    screen.setText("Passwords match");
                else
                    screen.setText("Passwords don't match");                //User gets a notification if the passwords don't match
            }
        });

        final Button registerButton = (Button) findViewById(R.id.registerButton);       //The button which tries to set the data of the new user
        registerButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                String name = nameET.getText().toString();                              //All info are converted to strings
                String email = emailET.getText().toString();
                String phone = phoneET.getText().toString();
                String homeCenter = homeSpinner.getSelectedItem().toString();
                String password1 = password1ET.getText().toString();
                String password2 = password2ET.getText().toString();
                if (name.isEmpty() || email.isEmpty() || phone.isEmpty() || homeCenter.isEmpty() || password1.isEmpty() || password2.isEmpty()) {
                    screen.setText("Information missing");                  //All info must be found before registeration
                    toast("Check all info");
                } else if (!email.contains("@")) {                          //Email must meet the requirements
                    screen.setText("Invalid email address");
                    toast("Check all info");
                } else if (!checkUsers(email)) {                            //Email must be unique
                    screen.setText("Email already in use");
                    toast("Check all info");
                } else if (homeCenter.equals("Choose primary sportshall")) {    //Primary sportshall must be chosen
                    screen.setText("Select your primary sportshall");
                    toast("Check all info");
                } else if (!checkPassword(password1)) {                      //Password must be valid
                    screen.setText("Invalid password");
                toast("Check all info");
                } else if (!comparePasswords(password1, password2)) {        //Passwords must match
                    screen.setText("Passwords don't match");
                    toast("Check all info");
                } else {                                                     //If all info is found and valid
                    screen.setText(" ");
                    temp.setInformation(name, email, phone, password2, homeCenter);     //We set the information to the empty object
                    toast("Account created");
                    Intent intent = new Intent();
                    intent.putExtra("olio", temp);              //After registeration, we return the new user object
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }
    public static Boolean checkPassword(String password) {      //Checks if the password meets the requirements
        Pattern specialPatten = Pattern.compile("[^A-Za-z0-9\\s]", Pattern.CASE_INSENSITIVE);
        if (password.length() < 12)                             //Password must be at least 12 characters long
            return false;
        else if (!specialPatten.matcher(password).find())       //Password must contain a special charachter
            return false;
        else {
            for (int i = 0; i < password.length(); i++) {       //Password must contain a capital letter
                if (Character.isUpperCase(password.charAt(i)))
                    break;
                else if (i == password.length()-1)
                    return false;
            }
            for (int i = 0; i < password.length(); i++) {       //Password must contain a lower case letter
                if (Character.isLowerCase(password.charAt(i)))
                    break;
                else if (i == password.length()-1)
                    return false;
            }
            for (int i = 0; i < password.length(); i++) {       //Password must contain a number
                if (Character.isDigit(password.charAt(i)))
                    break;
                else if (i == password.length()-1)
                    return false;                               //Returns false if all requirements aren't met
            }
        }
        return true;                                            //Returns true if the password is valid
    }

    public Boolean comparePasswords(String password1, String password2) {       //Returns true if the two given passwords match
        if (password1.equals(password2))
            return true;
        else
            return false;                                                       //Returns false if the passwords don't match
    }

    public Boolean checkUsers(String email) {      //Gets email as input and checks if there is already a user with the same email
        ArrayList<User> userList = UserControl.getUserList();
        for (User user : userList) {               //Goes through the user list
            if (user.getEmail().equals(email))
                return false;                       //Returns false if the email is already taken
        }
        return true;                                //Returns true if the email is free
    }

    public void toast(String msg) {     //Method used for informing the user by toasting
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
