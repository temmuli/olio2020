package com.example.SportsHall;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

public class SportsHall {
    private String name = "";
    private String city = "";
    private String location = "";
    private String openingHours = "";
    private String phoneNumber = "";
    private ArrayList<Reservation> reservationList = new ArrayList<>();     //Each sportshall has its own reservations in a list

    public SportsHall(String name, String city, String location, String openingHours, String phoneNumber) {
        this.name = name;
        this.city = city;
        this.location = location;
        this.openingHours = openingHours;
        this.phoneNumber = phoneNumber;
    }

    public String getCity() {
        return city;
    }
    public String getLocation() { return location; }
    public String getName() { return name; }
    public String getOpeningHours() { return openingHours; }
    public String getPhoneNumber(){return phoneNumber; }
    public ArrayList<Reservation> getThisHallReservationList(){
        return reservationList;
    }

    @Override
    public String toString() {
        return name;
    }

    public void makeReservation (Reservation res){      //Method to add a new reservation to the hall's list
        reservationList.add(res);
        JsonIO IO = JsonIO.getInstance();
        try {
            IO.writeListToFile(MainActivity.getListOfHalls());  //Saving the reservation list to the Json file
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }
}
