package com.example.SportsHall;


public abstract class Sport {       //We create sport as abstract class and then define Default and Created sport child classes
    private String name;
    private int maxPlayers;

    public Sport() {}
    public Sport(String name, int maxPlayers) {
        this.name = name;
        this.maxPlayers = maxPlayers;
    }
    public String getName() {
        return name;
    }
    public int getMaxPlayers() {
        return maxPlayers;
    }
    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }
}

// Default class for Sport
class DefaultSport extends Sport {
    public DefaultSport(Sport sport) {
        super(sport.getName(), sport.getMaxPlayers());
    }
    public DefaultSport(String name, int maxPlayers) {
        super(name, maxPlayers);
    }

    @Override
    public String toString() {
        return this.getName();
    }
}

// Extending Sport with created Sport, gets creator email
class CreatedSport extends Sport {
    private String creator;

    public CreatedSport(String name, int maxPlayers, String creator) {
        super(name, maxPlayers);
        this.creator = creator;
    }
    public String getCreator() {
        return creator;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}


