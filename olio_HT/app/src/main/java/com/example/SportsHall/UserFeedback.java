package com.example.SportsHall;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class UserFeedback extends AppCompatActivity {
    private Context context;
    private EditText titleET, textET;
    private Spinner sportshallSpinner;
    private ArrayList<String> hallList;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_feedback);
        context = UserFeedback.this;

        try {
            Bundle bundle = getIntent().getExtras();
            user = (User) bundle.getSerializable("olio");    //Gets the logged in user as input
            hallList = bundle.getStringArrayList("list");    //Gets the list of hall names
        } catch(NullPointerException e) {
            e.printStackTrace();
            user = new User();
            hallList = new ArrayList<>();
        } finally {
            hallList.add(0, "Choose sportshall for feedback");      //Adding the header to the hall list
            hallList.add("Overall feedback");
        }

        titleET = (EditText) findViewById(R.id.titleET);
        textET = (EditText) findViewById(R.id.textET);

        //Spinner where the user selects the sportshall that the feedback is about, or gives overall feedback
        sportshallSpinner = (Spinner) findViewById(R.id.sportshallSpinner);
        ArrayAdapter<String> hallAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, hallList);
        hallAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sportshallSpinner.setAdapter(hallAdapter);

        //Button for checking if all info is found and sending the feedback
        Button sendButton = (Button) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (titleET.getText().toString().isEmpty()) {
                    toast("Title missing");
                } else if (textET.getText().toString().isEmpty()) {
                    toast("Text missing");
                } else if (sportshallSpinner.getSelectedItem().toString().equals("Choose sportshall for feedback")) {
                    toast("Select a sportshall");
                } else {
                    LocalDateTime today = LocalDateTime.now(ZoneId.of("Europe/Helsinki"));  //Getting the current time for the feedback
                    FeedbackControl.addToList(new Feedback(today.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")),
                            titleET.getText().toString(), textET.getText().toString(),
                            sportshallSpinner.getSelectedItem().toString(), user.getEmail()));
                    toast("Feedback sent");
                    finish();
                }
            }
        });
    }

    public void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
