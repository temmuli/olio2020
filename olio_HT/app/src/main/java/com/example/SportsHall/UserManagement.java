package com.example.SportsHall;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class UserManagement extends AppCompatActivity {
    private Context context;
    private TextView infoTV;
    private User temp;
    private String activity;
    private ArrayList<User> spinnerList, userList;
    private Spinner userSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_management);
        context = UserManagement.this;

        infoTV = (TextView) findViewById(R.id.infoTV);
        final Button deleteButton = (Button) findViewById(R.id.delete);     //Buttons for deleting users
        final Button cancelButton = (Button) findViewById(R.id.cancel);
        deleteButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);

        //Spinner which contains all users in the system and selecting one shows all user's information
        spinnerList = new ArrayList<>();    //List of users to show in the spinner
        userSpinner = (Spinner) findViewById(R.id.userSpinner);
        ArrayAdapter<User> userAdapter = new ArrayAdapter<User>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, spinnerList);
        userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        updateSpinner();        //Method for updating the user spinner
        userSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).toString().equals("Select user")) {
                    setTVText();        //Method for showing a default text if user isn't selected
                } else {
                    for (User user : spinnerList) {
                        if (user.getName().equals(parent.getItemAtPosition(position).toString())) {
                            temp = user;
                            if (temp.getIsActive()) {
                                activity = "Active";        //Simplifying the isActive Boolean --> Active / Inactive
                            } else if (!temp.getIsActive()) {
                                activity = "Inactive";
                            }   //Showing the selected users information
                            infoTV.setText("Name: "+temp.getName() +"\nEmail: "+temp.getEmail()+"\nPassword: "+temp.getPassword()
                                    +"\nPhone: "+temp.getPhone()+"\nHome center: "+temp.getHomeCenter()+"\nLocation constraint: "
                                    +temp.getLocationConstraint()+"\n\nActivity: "+activity);
                        }
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        //Button for changing the selected user's activity, freezing / unfreezing
        Button activityButton = (Button) findViewById(R.id.activityButton);
        activityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (infoTV.getText().toString().equals("User information") || infoTV.getText().toString().equals("No users available")) {
                    toast("Select a user");
                } else {
                    temp = (User) userSpinner.getItemAtPosition(userSpinner.getSelectedItemPosition());
                    if (temp.getIsActive()) {
                        if (UserControl.changeActivity(temp, false)) {
                            updateSpinner();    //Updating the userSpinner after making changes
                            toast("Activity changed");
                        } else
                            toast("Activity change failed");        //If the user wasn't found for some reason
                    } else if (!temp.getIsActive()) {
                        if (UserControl.changeActivity(temp, true)) {
                            updateSpinner();
                            toast("Activity changed");
                        } else
                            toast("Activity change failed");
                    }
                }
            }
        });

        //Button for removing the selected user
        Button removeButton = (Button) findViewById(R.id.removeButton);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (infoTV.getText().toString().equals("User information") || infoTV.getText().toString().equals("No users available")) {
                    toast("Select a user");
                } else {
                    deleteButton.setVisibility(View.VISIBLE);   //Setting the confrimation buttons visible
                    cancelButton.setVisibility(View.VISIBLE);
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {       //Button for confirming the deletion
                temp = (User) userSpinner.getItemAtPosition(userSpinner.getSelectedItemPosition());
                if (UserControl.removeUser(temp)) {
                    updateSpinner();
                    toast("User removed");
                } else if (!UserControl.removeUser(temp)) {
                    toast("Remove failed");
                }
                deleteButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {       //Button for cancelling the action
                toast("Remove cancelled");
                deleteButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    public void setTVText() {               //Method for setting a default text on the screen if no users are found or selected
        if (spinnerList.size() == 1) {
            infoTV.setText("No users available");
        } else {
            infoTV.setText("User information");
        }
    }

    public void updateSpinner() {       //Method for setting the user spinner up to date
        spinnerList.clear();
        userList = UserControl.getUserList();
        spinnerList.add(0, new User("Select user","","","",""));
        spinnerList.addAll(userList);
        userSpinner.setAdapter(new ArrayAdapter<User>(context, android.R.layout.simple_list_item_1, spinnerList));
        setTVText();
    }

    public void toast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();       //We return the count of active users to the admin menu
        intent.putExtra("activeCount", UserControl.getActiveUserCount());
        setResult(RESULT_OK, intent);
        finish();
    }
}
