package com.example.SportsHall;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

public class AdminReservationManagement extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_reservation_management);
        context = AdminReservationManagement.this;

        // Reservation list is used to show all reservations
        // Adapter for listView using CustomArrayAdapter-class
        final ArrayList<Reservation> resList = getAllReservations();    //getAllReservations returns a list of all reservations found
        final CustomArrayAdapter adapter = new CustomArrayAdapter(context,android.R.layout.simple_list_item_1,resList);
        if (adapter.getCount() == 0) {
            toast("No reservations found");
        }

        final ListView LV = (ListView) findViewById(R.id.reservationsLV);
        LV.setAdapter(adapter);

        // OnClickListener for ListView, opens a dialog displaying reservation object data when item is clicked
        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                TextView resInfoTV = new TextView(context);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Selected reservation");
                builder.setView(resInfoTV);
                resInfoTV.setText(resList.get(position).listData());    //Showing the reservation's info

                // Delete-button for dialog
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Display new dialog to confirm deletion
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Delete item?");
                        TextView deleteInfo = new TextView(context);
                        builder.setView(deleteInfo);
                        deleteInfo.setText(resList.get(position).listData());

                        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteReservation(resList.get(position));   //Method to delete the reservation and update the json file
                                resList.remove(resList.get(position));
                                LV.setAdapter(adapter);         //Updating the reservations in the list view
                                toast("Reservation deleted successfully");
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {    //The delete can be cancelled
                                dialog.cancel();
                            }
                        });
                        builder.show();
                    }
                });

                // Change sport button for dialog to modify reservation
                builder.setNeutralButton("Change sport", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        // Display current value in the title
                        builder.setTitle("Change sport, current: "+resList.get(position).getSport().getName());
                        final Spinner sport = new Spinner(context);     //Spinner is used to select a new sport for the reservation
                        builder.setView(sport);
                        ArrayAdapter<Sport> sportAdapter = new ArrayAdapter<Sport>(context, android.R.layout.simple_spinner_item, SportControl.getSportList());
                        sportAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sport.setAdapter(sportAdapter);

                        // Confirm button, modify reservation with given value, update ListView to match changes made
                        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                modifyReservation(resList.get(position),SportControl.getSportList().get(sport.getSelectedItemPosition()));
                                resList.get(position).setSport(SportControl.getSportList().get(sport.getSelectedItemPosition()));
                                LV.setAdapter(adapter);
                                toast("Reservation updated successfully");
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.show();
                    }
                });

                // Negative button for dialog to cancel it
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
    }

    // Method returns a list of all reservations in all sportshalls
    private ArrayList<Reservation> getAllReservations(){
        ArrayList<Reservation> allReservations = new ArrayList<Reservation>();
        JsonIO IO = JsonIO.getInstance();
        try {
            IO.readToList(MainActivity.getListOfHalls(), SportControl.getSportList());   //Reading the reservations from Json file to each hall's list
        } catch (JSONException | IOException | ParseException e) {
            e.printStackTrace();
        }
        for (SportsHall sh : MainActivity.getListOfHalls()){             //Every sportshall
            allReservations.addAll(sh.getThisHallReservationList());     //Every reservation
        }
        return allReservations;         //Returning the full list
    }

    // Method deletes the passed reservation and writes list to file immediately to keep it up to date
    private void deleteReservation(Reservation res){
        JsonIO IO = JsonIO.getInstance();

        for(SportsHall sh:MainActivity.getListOfHalls()){
            for(int i = 0; i < sh.getThisHallReservationList().size(); i++){
                if(sh.getThisHallReservationList().get(i) == res){
                    sh.getThisHallReservationList().remove(i);                  //Removing the selected reservation
                    try {
                        IO.writeListToFile(MainActivity.getListOfHalls());      //Updating the Json file
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    // Method to modify the given reservation with a new sport, writes list to file to keep it up to date
    public void modifyReservation(Reservation res, Sport s){
        JsonIO IO = JsonIO.getInstance();

        for(SportsHall sh:MainActivity.getListOfHalls()){
            for(Reservation r:sh.getThisHallReservationList()){
                if(r == res){
                    sh.getThisHallReservationList().get(sh.getThisHallReservationList().indexOf(res)).setSport(s);  //Setting the new sport
                    try {
                        IO.writeListToFile(MainActivity.getListOfHalls());      //Updating the Json file
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void toast(String msg) {
        Toast.makeText(context, msg,Toast.LENGTH_LONG).show();
    }
}

