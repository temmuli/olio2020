package com.example.SportsHall;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

public class ManageResActivity extends AppCompatActivity {
    private Context context;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_res);

        // Declaring context and importing user
        context = ManageResActivity.this;
        Bundle bundle = getIntent().getExtras();
        user = (User) bundle.getSerializable("olio");

        // reservation list using method to find users reservations
        // Adapter for listView using CustomArrayAdapter-class
        final ArrayList<Reservation> resList = getUserReservations(user);
        final CustomArrayAdapter adapter = new CustomArrayAdapter(context,android.R.layout.simple_list_item_1,resList);
        if (adapter.getCount() == 0){
            Toast.makeText(context,"No reservations found, make one in previous menu!",Toast.LENGTH_LONG).show();
        }

        final ListView LV = (ListView) findViewById(R.id.reservationLV);
        LV.setAdapter(adapter);

        // OnClickListener for ListView, opens a dialog displaying reservation object data when item is clicked
        LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                TextView resInfoTV = new TextView(context);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Selected reservation");
                builder.setView(resInfoTV);
                resInfoTV.setText(resList.get(position).listData());

                // Delete-button for dialog
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(user.getEmail().equals(resList.get(position).getCreator())) {

                            // Display new dialog to confirm deletion
                            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Delete item?");
                            TextView deleteInfo = new TextView(context);
                            builder.setView(deleteInfo);
                            deleteInfo.setText(resList.get(position).listData());

                            builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteReservation(resList.get(position));
                                    resList.remove(resList.get(position));
                                    LV.setAdapter(adapter);
                                    Toast.makeText(context, "Reservation deleted successfully", Toast.LENGTH_LONG).show();
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        } else {
                            Toast.makeText(context,"Only the creator can delete a reservation",Toast.LENGTH_LONG).show();
                        }
                    }
                });

                // Change sport button for dialog to modify reservation
                builder.setNeutralButton("Change sport", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

                        // Display current value in the title
                        builder.setTitle("Change sport, current: "+resList.get(position).getSport().getName());

                        // Add sport-spinner to select new sport for reservation
                        final Spinner sport = new Spinner(context);
                        builder.setView(sport);
                        ArrayAdapter<Sport> sportAdapter = new ArrayAdapter<Sport>(context, android.R.layout.simple_spinner_item, SportControl.getSportList());
                        sportAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sport.setAdapter(sportAdapter);
                        sport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {}
                        });

                        // Confirm button, modify reservation with given value, update ListView to match changes made
                        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (user.getEmail().equals(resList.get(position).getCreator())) {
                                    modifyReservation(resList.get(position), SportControl.getSportList().get(sport.getSelectedItemPosition()));
                                    resList.get(position).setSport(SportControl.getSportList().get(sport.getSelectedItemPosition()));
                                    LV.setAdapter(adapter);
                                    Toast.makeText(context, "Reservation updated successfully", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(context,"Only the creator can modify a reservation",Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.show();
                    }
                });

                // Negative button for dialog to cancel it
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
    }

    // Return a list of reservations where email of reservation matches to that of given user
    private ArrayList<Reservation> getUserReservations(User user){
        ArrayList<Reservation> userReservations = new ArrayList<Reservation>();
        JsonIO IO = JsonIO.getInstance();
        try {
            IO.readToList(MainActivity.getListOfHalls(),SportControl.getSportList());
        } catch (JSONException | IOException | ParseException e) {
            e.printStackTrace();
        }
        for (SportsHall sh:MainActivity.getListOfHalls()){
            for (Reservation res:sh.getThisHallReservationList()){
                ArrayList<User> users = res.getUserList();
                for (User u:users){
                    if (u.getEmail().equals(user.getEmail())){
                        userReservations.add(res);
                    }
                }
            }
        }
        return userReservations;
    }

    // Deletes the passed reservation, writes list to file immediately to keep it up to date
    private void deleteReservation(Reservation res){
        JsonIO IO = JsonIO.getInstance();

        for(SportsHall sh:MainActivity.getListOfHalls()){
            for(int i = 0; i < sh.getThisHallReservationList().size(); i++){
                if(sh.getThisHallReservationList().get(i) == res){
                    sh.getThisHallReservationList().remove(i);
                    try {
                        IO.writeListToFile(MainActivity.getListOfHalls());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    // Modifies given reservation with a new sport, writes list to file to keep it up to date
    public void modifyReservation(Reservation res, Sport s){
        JsonIO IO = JsonIO.getInstance();

        for(SportsHall sh:MainActivity.getListOfHalls()){
            for(Reservation r:sh.getThisHallReservationList()){
                if(r == res){
                    sh.getThisHallReservationList().get(sh.getThisHallReservationList().indexOf(res)).setSport(s);
                    try {
                        IO.writeListToFile(MainActivity.getListOfHalls());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}




