package com.example.SportsHall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class MainNormalActivity extends AppCompatActivity {
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    private Button makeReservationButton;
    private Button manageResButton;
    private Button checkResButton;
    private User user, temp;
    private Context context;
    private ArrayList<String> hallList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_normal);
        context = MainNormalActivity.this;

        Bundle bundle = getIntent().getExtras();
        user = (User) bundle.getSerializable("olio");    //Gets logged in user-object as parameter
        hallList = bundle.getStringArrayList("list");    //Gets list of hall names

        toast("Welcome "+user.getName());

        // DrawerLayout and toggle button declarations
        dl = (DrawerLayout)findViewById(R.id.activity_main);
        t = new ActionBarDrawerToggle(this, dl,R.string.Open, R.string.Close);
        dl.addDrawerListener(t);
        t.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Button declarations
        makeReservationButton = (Button) findViewById(R.id.makeReservationButton);
        makeReservationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMakeReservation();
            }
        });

        manageResButton = (Button) findViewById(R.id.manageReservationButton);
        manageResButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToManageReservation();
            }
        });

        checkResButton = (Button)findViewById(R.id.checkReservationButton);
        checkResButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCheckReservation();
            }
        });

        // Side navigation drawer declaration and onclicklistener
        nv = (NavigationView)findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch(id)
                {
                    case R.id.feedback:
                        goToFeedback();
                        break;
                    case R.id.account:
                        goToOwnProfile(user);
                        break;
                    case R.id.logout:

                        // Logout confirmation dialog, finish activity when confirmed to block getting back by pressing back button
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainNormalActivity.this);
                        builder.setTitle("Log out?");
                        builder.setPositiveButton("Log out", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                goToEntry();
                                finish();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.show();
                        break;
                    default:
                        return true;
                }
                return true;
            }
        });
    }

    // Take user back to entry "mainActivity"
    public void goToEntry(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    // Start makeReservation activity, pass user by bundle
    public void goToMakeReservation() {
        Intent intent = new Intent(this, MakeReservation.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("olio", user);
        bundle.putStringArrayList("list", hallList);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    // Start feedback managing activity, pass user by bundle
    public void goToFeedback() {
        Intent intent = new Intent(this, UserFeedback.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("olio", user);
        bundle.putStringArrayList("list", hallList);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    // Open own profile-activity, clone the user-object to change the attributes at own profile
    public void goToOwnProfile(User user){
        Intent intent = new Intent(this, OwnProfileActivity.class);
        try {
            temp = (User) user.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        intent.putExtra("olio", temp);
        startActivityForResult(intent, 1);
    }

    // Start reservation managing activity, pass user by bundle
    public void goToManageReservation(){
        Intent intent = new Intent(this, ManageResActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("olio", user);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    // Start check reservation activity, pass user by bundle
    public void goToCheckReservation(){
        Intent intent = new Intent(this, CheckReservationsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("olio", user);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(t.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                temp = (User) data.getSerializableExtra("olio");        //We catch the updated user-object
                if (UserControl.updateUserList(user, temp)) {                 //We delete the old user and add the updated one to the list
                    toast("Account updated");
                    user = temp;                                              //Finally we update the new user as the current one
                } else {
                    toast("Update failed");
                }
            }
        }
    }
    public void toast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}

