package com.example.SportsHall;

import java.util.ArrayList;

public class UserControl {
    private static ArrayList<User> userList = new ArrayList<>();     //List of all users

    private UserControl() {}
    private static UserControl control = new UserControl();         //We use static singleton class
    public static UserControl getInstance() {
        return control;
    }

    public static void addToList(User user) {       //Method to add a new user to the user list and saving the list to xml file
        userList.add(user);
        saveToFile();
    }
    public static ArrayList<User> getUserList() {
        return userList;
    }

    public static Boolean updateUserList(User oldUser, User newUser) {  //Method to update the user's info when it's changed
        for (User u : userList) {
            if (u.getEmail().equals(oldUser.getEmail())) {
                userList.remove(u);                             //Removing the old user
                addToList(newUser);                             //Adding the updated one
                return true;
            }
        }
        return false;                                           //Returning false if the user wasn't found
    }

    public static int getActiveUserCount() {        //Method that returns the count of active non-admin users
        int count = 0;
        for (User u : userList) {
            if (!u.getIsAdmin()) {
                if (u.getIsActive())
                    count++;
            }
        }
        return count;
    }

    public static Boolean removeUser(User user) {       //Method for admin to remove a user from the list
        for (User u : userList) {
            if (u.getEmail().equals(user.getEmail())) {
                userList.remove(u);
                saveToFile();
                return true;
            }
        }
        return false;                                   //Returning false if the user wasn't found
    }

    public static Boolean changeActivity(User user, Boolean activity) {     //Method for admin to freeze/unfreeze an user's account
        for (User u : userList) {
            if (u.getEmail().equals(user.getEmail())) {
                u.setActivity(activity);
                saveToFile();
                return true;
            }
        }
        return false;
    }

    public static Boolean readToList() {        //Method to read the users from xml file to an ArrayList
        ArrayList<User> tempList = Xmlio.readUserXML();
        if (tempList == null) {
            return false;
        } else {
            userList.clear();
            userList.addAll(tempList);
            return true;
        }
    }

    public static void saveToFile() {       //Method to save the user list to xml file
        Xmlio.writeUserXML(userList);
    }
}
