package com.example.SportsHall;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AdminProfileActivity extends AppCompatActivity {
    private User user;
    private Context context;
    private EditText newPassFirst, newPassSecond, oldPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_profile);
        context = AdminProfileActivity.this;

        user = (User) getIntent().getSerializableExtra("olio");     //Gets logged in user as input

        final TextView ownDataTV = (TextView) findViewById(R.id.ownData);
        final TextView homeCenterTV = (TextView) findViewById(R.id.HomeCenter);

        String userData = user.getAllDisplayableData();     //Getting the user's data
        ownDataTV.setText(userData);                        //Showing the information

        String homeCenter = "Home center: "+"\t\t"+user.getHomeCenter();
        homeCenterTV.setText(homeCenter);

        final Button mod = (Button) findViewById(R.id.modButton);       //Button to modify the information (name, email and phone number)
        final String[] dataItems = {"Name", "Email", "Phone"};
        mod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select data to update");
                builder.setSingleChoiceItems(dataItems,0,null);     //Showing the options to change

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {    //If the user want's to confirm the changes
                        dialog.dismiss();
                        final int selected = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                        builder2.setTitle("Updating "+dataItems[selected]+", old value: "+user.getDataByIndex(selected));   //Showing the old value

                        final EditText input = new EditText(context);
                        input.setHint("New Value: ");                       //Asking for the new value
                        input.setInputType(InputType.TYPE_CLASS_TEXT);
                        builder2.setView(input);

                        builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {    //Asking for confirmation
                                user.setDataByIndex(selected,input.getText().toString());
                                ownDataTV.setText(user.getAllDisplayableData());
                            }
                        });
                        builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {    //User can still cancel the modification
                                dialog.cancel();
                            }
                        });
                        builder2.show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {    //If the user want's to cancel the modification
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

        final Button mod2 = (Button) findViewById(R.id.modButton2);     //Button to update the home sportshall
        mod2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {                                 //Done in different button because we use the spinner for sportshalls
                final Spinner hallSelection = new Spinner(context);
                final ArrayAdapter<SportsHall> itemList = new ArrayAdapter<SportsHall>(context, android.R.layout.simple_spinner_item, MainActivity.getListOfHalls());
                hallSelection.setAdapter(itemList);     //Setting up the sportshall spinner

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select new home center below, current hall: "+user.getHomeCenter());
                builder.setView(hallSelection);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {        //Setting the new primary sportscenter
                        user.setHomeCenter(hallSelection.getSelectedItem().toString());
                        homeCenterTV.setText("Home center: "+"\t\t"+user.getHomeCenter());
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {    //Modification can still be cancelled
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

        final Button modPassword = (Button) findViewById(R.id.modButton3);      //Button to change the password
        modPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Change password");
                oldPassword = new EditText(context);
                oldPassword.setHint("Current password:");       //We ask the current password before letting the user change it
                oldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                builder.setView(oldPassword);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {    //If the user wants to change it
                        if (oldPassword.getText().toString().equals(user.getPassword())) {  //Checking the passwords correctness
                            AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                            builder2.setTitle("Give new password");

                            LinearLayout layout = new LinearLayout(context);
                            layout.setOrientation(LinearLayout.VERTICAL);

                            newPassFirst = new EditText(context);       //We ask the new password twice
                            newPassFirst.setHint("New password:");
                            newPassFirst.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            newPassSecond = new EditText(context);
                            newPassSecond.setHint("New password again:");
                            newPassSecond.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                            layout.addView(newPassFirst);
                            layout.addView(newPassSecond);

                            builder2.setView(layout);

                            builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {    //Confirming the password change
                                    String newPass1 = newPassFirst.getText().toString();
                                    String newPass2 = newPassSecond.getText().toString();
                                    if (RegisterActivity.checkPassword(newPass1) && newPass1.equals(newPass2)) {    //Checking if the passwords are the same and  meet the requirements
                                        Toast.makeText(context,"Password changed!",Toast.LENGTH_LONG).show();
                                        user.setPassword(newPass1);     //Setting the new password
                                    } else {
                                        Toast.makeText(context,"Password change failed!",Toast.LENGTH_LONG).show();
                                        dialog.cancel();
                                    }
                                }
                            });

                            builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder2.show();
                        } else {
                            Toast.makeText(context,"Wrong password",Toast.LENGTH_LONG).show();
                            dialog.cancel();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("olio", user);      //When returning to main menu, the updated object is also returned
        setResult(RESULT_OK, intent);
        finish();
    }
}
